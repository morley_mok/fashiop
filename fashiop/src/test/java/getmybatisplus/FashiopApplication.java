package getmybatisplus;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 应用程序入口类
 * 
 * @author 莫尚荣
 *
 */
@SpringBootApplication
@MapperScan({"top.shenyuboos.fashiop.product.mapper","top.shenyuboos.fashiop.order.mapper","top.shenyuboos.fashiop.system.mapper","top.shenyuboos.fashiop.user.mapper"})
public class FashiopApplication {

	public static void main(String[] args) {
		SpringApplication.run(FashiopApplication.class, args);
	}

}
