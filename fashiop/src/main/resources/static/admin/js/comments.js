$(function() {
	$("#all").on("click",function(){
		if(this.checked){
			$("input[name='checkbox']").attr("checked",true);
		}else{
			$("input[name='checkbox']").attr("checked",false);
		}
		
	});
	
	$("[name=delall]").click(function() {
		var $tr = $("input[name='checkbox']:checked").parents("tr");
		var checkedNum =$("input[name='checkbox']:checked").length;
		if(checkedNum==0){
			layer.msg('请至少选择一项', {icon: 2});
			return;
		}
		   layer.confirm('确认要删除吗？',function(index){
		       var checkList =new Array();
		    $.each($("input[name='checkbox']:checked"),function(){
                checkList.push($(this).parent().next().html());
                  
            });
				$.ajax({
					type:"POST",
					url:"/product/os-comment/dels",
					data:{commentId:checkList.toString()},
					success:function(data){
						if(data==true){
							$("[name ='checkbox']:checkbox").attr("checked", false); 
							 $tr.remove();
						     layer.msg('已删除!',{icon:1,time:1000});
						     $.ajax({
									type:"POST",
									url:"/product/os-comment/getCounts",
									dataType:"json",
									success:function(data){
										$("#count").text(data);
									}
									});
						}else{
							layer.msg('删除失败', {icon: 2});
						}
					}
				});
		   });
	});
	
	$("[name=del]").click(function() {
		var $tr = $(this).parents("tr");
		var $id=$(this).parent().parent().children('td').eq(1).text();
		 layer.confirm('确认要删除吗？',function(index){
			$.ajax({
				type:"POST",
				url:"/product/os-comment/del",
				data:{commentId:$id},
				success:function(data){
					if(data==true){
						 $tr.remove();
					     layer.msg('已删除!',{icon:1,time:1000});
					     $.ajax({
								type:"POST",
								url:"/product/os-comment/getCounts",
								dataType:"json",
								success:function(data){
									$("#count").text(data);
								}
								});
					}else{
						layer.msg('删除失败', {icon: 2});
					}
				
				}
			});
		 });
	});
});
/*用户-删除*/
function member_del(obj,id){
    layer.confirm('确认要删除吗？',function(index){
        //发异步删除数据
        $(obj).parents("tr").remove();
        layer.msg('已删除!',{icon:1,time:1000});
    });
}
//批量删除提交
function delAll () {
   layer.confirm('确认要删除吗？',function(index){
       layer.msg('删除成功', {icon: 1});
   });
}