var specIndex = 0;
layui.use('form', function(){
	var form = layui.form(); //获取form模块
	
	
	form.on('select(spec_name)', function(data) {
		if(data.value == 0) {
			$("#p_spec_name").show();
			
			//移除属性值
			$("[name=spec_attr]").children().not($(".def")).remove();
			form.render('select');
		} else {
			$("#p_spec_name").hide();
			
			//根据属性名查属性值
			var specAttr = "";
			$.ajax({
				type:"POST",
				url:"/product/os-specification-attribute/getBySpecId",
				data:{specId:data.value},
				dataType:"json",
				success:function(data){
					$.each(data,function(i,e) {
						specAttr += "<option value='"+e.specAttrId+"'>"+e.name+"</option>";
					});
					$("[name=spec_attr]").children().not($(".def")).remove();
					$("[name=spec_attr]").append(specAttr);
					form.render('select');
				}
			});
		}		
	});
	
	form.on('select(spec_attr)', function(data) {
		if(data.value == 0) {
			$("#p_spec_attr").show();
		} else {
			$("#p_spec_attr").hide();
		}
	});
	
	/**
	*添加属性节点
	*/
	$(".addSpec").click(function(){			
		var html = "<div class='layui-form-item'>" +
		"<label class='layui-form-label'> 选择属性 </label>" +
		"<div class='layui-input-inline'>" +
			"<select name='item_spec_name"+specIndex+"' lay-verify='label'" +
				"lay-filter='item_spec_name"+specIndex+"'>" +
			"</select> <input style='display: none;' type='text' id='p_spec_name"+specIndex+"'" +
				"autocomplete='off' class='layui-input' />" +
		"</div>" +
		"<div class='layui-input-inline'>" +
			"<select name='item_spec_attr"+specIndex+"' lay-filter='item_spec_attr"+specIndex+"'>" +
				"<option value='' class='def'>--属性值--</option>" +
				"<option value='0' class='def'>手动添加</option>" +
			"</select> <input style='display: none;' type='text' id='p_spec_attr"+specIndex+"'" +
				"autocomplete='off' class='layui-input' />" +
		"</div>" +
		"<div class='layui-input-inline'>" +
			"<input type='number' id='p_price' name='p_price+"+specIndex+"' required=''" +
				"placeholder='价格' lay-verify='number|required' autocomplete='off'" +
				"class='layui-input'>" +
		"</div>" +
	"</div>";
		
		var specDiv = $(this).parent().parent();
		specDiv.after(html);
		
		var specName = $("[name=spec_name]").html();
		
		$("[name=item_spec_name"+specIndex+"]").append(specName);
		
		form.render('select');
		
		var selName = "select(item_spec_name"+specIndex+")";
		form.on(selName, function(data) {
			var $itemSpecAttr = $(data.elem).parent().next().children("select");
			if(data.value == 0) {
				$(data.elem).siblings("input").show();
				
				//移除属性值
				$itemSpecAttr.children().not($(".def")).remove();
				form.render('select');
			} else {
				$(data.elem).siblings("input").hide();
				
				//根据属性名查属性值
				var specAttr = "";
				$.ajax({
					type:"POST",
					url:"/product/os-specification-attribute/getBySpecId",
					data:{specId:data.value},
					dataType:"json",
					success:function(data){
						$.each(data,function(i,e) {
							specAttr += "<option value='"+e.specAttrId+"'>"+e.name+"</option>";
						});
						$itemSpecAttr.children().not($(".def")).remove();
						$itemSpecAttr.append(specAttr);
						form.render('select');
					}
				});
			}		
		});
		
		form.on("select(item_spec_attr"+specIndex+")", function(data) {
			if(data.value == 0) {
				$(data.elem).siblings("input").show();
			} else {
				$(data.elem).siblings("input").hide();
			}
		});
		
		specIndex ++;
	});
	
	form.on('submit(add)', function(data) {
		return true;
	});
	
	var imgNum = 1;
	$(".addImg").click(function() {
		if(imgNum==4){
			layer.msg('最多只能添加4张图片',{icon:1,time:1000});
			return;
		}
		imgNum++;
		$(this).parent().prev().append("<div class='layui-input-inline'>"+
											"选择详细图片: <input type='file' name='detaImg' />"+
										"</div>");
		
	});
});