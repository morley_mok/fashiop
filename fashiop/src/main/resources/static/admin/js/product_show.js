$(function() {
	var specId = new Array();
	$(".specName").each(function(i,e) {
		specId[i] = $(e).attr("spec");
	});
	$.ajax({
		url:"/product/os-product-specification/getSpecs",
		type:"POST",
		data:{specId:specId},
		dataType:"json",
		success:function(data){
			var s1 = data[specId[0]];
			var s2 = data[specId[1]];
			$(".specName").each(function(i,e) {
				var spec = "";
				$.each(data[$(e).attr("spec")].osSpecificationAttribute, function(i,e) {
					spec += e.osSpecification.name + ":" + e.name + "<br />";
				});
				$(e).html(spec);
			});
		}
	});
});