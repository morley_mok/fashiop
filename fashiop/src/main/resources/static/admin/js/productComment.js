layui.use(['laydate'], function(){
  laydate = layui.laydate;//日期插件

  //以上模块根据需要引入
  //
  

  
  var start = {
    min: laydate.now()
    ,max: '2099-06-16 23:59:59'
    ,istoday: false
    ,choose: function(datas){
      end.min = datas; //开始日选好后，重置结束日的最小日期
      end.start = datas //将结束日的初始值设定为开始日
    }
  };
  
  var end = {
    min: laydate.now()
    ,max: '2099-06-16 23:59:59'
    ,istoday: false
    ,choose: function(datas){
      start.max = datas; //结束日选好后，重置开始日的最大日期
    }
  };
  
});

/*用户-查看*/
function member_show(title,url,id,w,h){
    x_admin_show(title,url+'?productId='+id,w,h);
}

function getCount() {
	$.ajax({
		type:"POST",
		url:"/product/os-comment/getCount",
		dataType:"json",
		success:function(data){
			return data;
		}
	});
}

var count = parseInt($("#count").text());
$(function () {
	layui.use(['laypage', 'layer'], function(){
  	  var laypage = layui.laypage
  	  ,layer = layui.layer;
  	  var pageCount = count%10==0?count/10:count/10+1;
  	  layui.laypage({
        	cont: 'pages',
        	pages: pageCount,
        	jump:function(obj, first) {
        		var productNumber = "%" + $("[name=productNumber]").val() + "%";
        		var minPrice = $("[name=minPrice]").val();
        		var maxPrice = $("[name=maxPrice]").val();
        		var searchKey = "%" + $("[name=searchKey]").val() + "%";
        		var current = obj.curr;
				$.ajax({
					type:"POST",
					url:"/product/os-comment/getCommentList",
					data:{productNumber:productNumber,minPrice:minPrice,maxPrice:maxPrice,searchKey:searchKey,current:current},
					datatype:"json",
					success:function(data){
						setList(data);
					}
				});	
        	}
      });
	});
	/*
	 * 搜索按钮点击事件
	 */
	$(".xbs").submit(function() {
		var productNumber = "%" + $("[name=productNumber]").val() + "%";
		var minPrice = $("[name=minPrice]").val();
		var maxPrice = $("[name=maxPrice]").val();
		var searchKey = "%" + $("[name=searchKey]").val() + "%";
		
		$.ajax({
			type:"POST",
			url:"/product/os-comment/getCommentList",
			data:{productNumber:productNumber,minPrice:minPrice,maxPrice:maxPrice,searchKey:searchKey},
			dataType:"json",
			success:function(data){
				setList(data);
				$.ajax({
					type:"POST",
					url:"/product/os-comment/getCount",
					dataType:"json",
					success:function(data){
						$("#count").text(data);
						/**
						 * 分页
						 */
						var $page = $("#pages");
						$page.empty();
						layui.use(['laypage', 'layer'], function(){
					    	  var laypage = layui.laypage
					    	  ,layer = layui.layer;
					    	  var pageCount = data%10==0?data/10:data/10+1;
					    	  layui.laypage({
					          	cont: 'pages',
					          	pages: pageCount,
					          	jump:function(obj, first) {
					          		var productNumber = "%" + $("[name=productNumber]").val() + "%";
					          		var minPrice = $("[name=minPrice]").val();
					          		var maxPrice = $("[name=maxPrice]").val();
					          		var searchKey = "%" + $("[name=searchKey]").val() + "%";
					          		var current = obj.curr;
					          		$.ajax({
					          			type:"POST",
					          			url:"/product/os-comment/getCommentList",
					          			data:{productNumber:productNumber,minPrice:minPrice,maxPrice:maxPrice,searchKey:searchKey,current:current},
					          			dataType:"json",
					          			success:function(data){
					          				setlist(data);
					          			}
					          		});
					          	}
					          });
					    	});
					}
				});
			}
		});
		return false;
	});
	
	function setList(data) {
		var $tr="";
		$(data).each(function(i,e){
			var shelveTime = e.shelveTime == null ? "":e.shelveTime
					var showInShelve = "已上架";
					if(e.showInShelve == 0) {
						showInShelve = "已下架";
					}
					$tr +="<tr>"+
							"<td id='productId'>" +e.productId +"</td>" +
							"<td>" +e.productNumber +"</td>" +
							"<td>" +e.commentCounts+"</td>"+
							"<td>" +e.name +"</td>" +
							"<td>" +e.showPrice +"</td>" +
							"<td>" +shelveTime +"</td>" +
							"<td>" +
							"<span class='layui-btn layui-btn-normal layui-btn-mini'>" +
							showInShelve +
							"</span>" +
							"</td>" +
							"<td class='td-status'>" +
							"<u name='xq' style='cursor:pointer' onclick=\"member_show('评论详情','/admin/comments','"+e.productId+"','360','400')\"	>" +
							"详情" +
							"</u>" +
							"</td>" +
							"</tr>";
		});
		$("#productComment").html($tr);
	}

});

