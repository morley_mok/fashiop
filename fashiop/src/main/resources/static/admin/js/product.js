layui.use(['laydate'], function(){
  laydate = layui.laydate;//日期插件

  //以上模块根据需要引入
  //
  

  
  var start = {
    min: laydate.now()
    ,max: '2099-06-16 23:59:59'
    ,istoday: false
    ,choose: function(datas){
      end.min = datas; //开始日选好后，重置结束日的最小日期
      end.start = datas //将结束日的初始值设定为开始日
    }
  };
  
  var end = {
    min: laydate.now()
    ,max: '2099-06-16 23:59:59'
    ,istoday: false
    ,choose: function(datas){
      start.max = datas; //结束日选好后，重置开始日的最大日期
    }
  };
  
});

/*用户-查看*/
function product_show(title,url,id,w,h){
    x_admin_show(title,url+'?productId='+id,w,h);
}

function getCount() {
	$.ajax({
		type:"POST",
		url:"/product/os-product/getCount",
		dataType:"json",
		success:function(data){
			return data;
		}
	});
}

var count = parseInt($("#count").text());

$(function() {	
	//全选
	$(".allCheck").change(function() {
		var isCheck = $(this).prop("checked");
		if(isCheck)
			$("input:not(:checked)").prop("checked",true);
		else
			$("input:checked").prop("checked",false);
	});
	
	
	/**
	 * 分页
	 */
	layui.use(['laypage', 'layer'], function(){
    	  var laypage = layui.laypage
    	  ,layer = layui.layer;
    	  var pageCount = count%10==0?count/10:count/10+1;
    	  layui.laypage({
          	cont: 'pages',
          	pages: pageCount,
          	jump:function(obj, first) {
          		var productNumber = "%" + $("[name=productNumber]").val() + "%";
          		var minPrice = $("[name=minPrice]").val();
          		var maxPrice = $("[name=maxPrice]").val();
          		var searchKey = "%" + $("[name=searchKey]").val() + "%";
          		var current = obj.curr;
          		$.ajax({
          			type:"POST",
          			url:"/product/os-product/getProductList",
          			data:{productNumber:productNumber,minPrice:minPrice,maxPrice:maxPrice,searchKey:searchKey,current:current},
          			dataType:"json",
          			success:function(data){
          				setlist(data);
          			}
          		});
          	}
          });
    	});
	
	/*
	 * 搜索按钮点击事件
	 */
	$(".xbs").submit(function() {
		var productNumber = "%" + $("[name=productNumber]").val() + "%";
		var minPrice = $("[name=minPrice]").val();
		var maxPrice = $("[name=maxPrice]").val();
		var searchKey = "%" + $("[name=searchKey]").val() + "%";
		
		$.ajax({
			type:"POST",
			url:"/product/os-product/getProductList",
			data:{productNumber:productNumber,minPrice:minPrice,maxPrice:maxPrice,searchKey:searchKey,searchProduct:true},
			dataType:"json",
			success:function(data){
				setlist(data);
				$.ajax({
					type:"POST",
					url:"/product/os-product/getCount",
					dataType:"json",
					success:function(data){
						$("#count").text(data);
						/**
						 * 分页
						 */
						var $page = $("#pages");
						$page.empty();
						layui.use(['laypage', 'layer'], function(){
					    	  var laypage = layui.laypage
					    	  ,layer = layui.layer;
					    	  var pageCount = data%10==0?data/10:data/10+1;
					    	  layui.laypage({
					          	cont: 'pages',
					          	pages: pageCount,
					          	jump:function(obj, first) {
					          		var productNumber = "%" + $("[name=productNumber]").val() + "%";
					          		var minPrice = $("[name=minPrice]").val();
					          		var maxPrice = $("[name=maxPrice]").val();
					          		var searchKey = "%" + $("[name=searchKey]").val() + "%";
					          		var current = obj.curr;
					          		$.ajax({
					          			type:"POST",
					          			url:"/product/os-product/getProductList",
					          			data:{productNumber:productNumber,minPrice:minPrice,maxPrice:maxPrice,searchKey:searchKey,current:current},
					          			dataType:"json",
					          			success:function(data){
					          				setlist(data);
					          			}
					          		});
					          	}
					          });
					    	});
					}
				});
			
			
			}
		});

		return false;
	});
});

/**
 * 生成商品列表
 * @returns
 */
function setlist(data) {
	var $tr = "";
	$(data).each(function(i ,e) {
		var labelName = typeof(e.osLabel)=="undefined" ? "":e.osLabel.labelName
		var shelveTime = e.shelveTime == null ? "":e.shelveTime
				
		var showInShelve = "已上架";
		var showInShelveIcon = "&#xe601;";
		var showInShelveText = "下架";
		var showInShelveColor = "layui-btn-normal";
		if(e.showInShelve == 0) {
			showInShelve = "已下架";
			showInShelveIcon = "&#xe608;";
			showInShelveText = "上架";
			showInShelveColor = "layui-btn-danger";
		}
		
		$tr += "<tr>" +
						"<td>" +
							"<input type='checkbox' value='1' name=''>" +
						"</td>" +
						"<td id='productId'>" +
							e.productId +
						"</td>" +
						"<td class='edit'>" +
						e.productNumber +
						"</td>" +
						"<td class='edit'>" +
						labelName +
						"</td>" +
						"<td class='edit'>" +
						e.name +
						"</td>" +
						"<td class='edit'>" +
						e.showPrice +
						"</td>" +
						"<td class='edit'>" +
						shelveTime +
						"</td>" +
						"<td>" +

						"<span class='layui-btn "+showInShelveColor+" layui-btn-mini showInShelve'>" +
						showInShelve +
						"</span>" +
						"</td>" +
						"<td class='td-status'>" +
							"<u style='cursor:pointer' onclick=\"product_show('" +e.name+ "','/admin/product-show','" +e.productId+"','700','600')\">" +
							"详情" +
							"</u>" +
						"</td>" +
						"<td class='td-manage'>"+
							"<a style='text-decoration:none' onclick=\"product_out(this,"+e.productId+")\" href='javascript:;' title='"+showInShelveText+"'>" +
								"<i class='layui-icon'>"+showInShelveIcon+"</i>" +
							"</a>" +
							"<a title='编辑' href='javascript:;' onclick='product_edit(this)'" +
								"class='ml-5' style='text-decoration:none'>" +
								"<i class='layui-icon'>&#xe642;</i>" +
							"</a>" +

							"<a title='删除' href='javascript:;' onclick='product_del(this,"+e.productId+")' " +
								"style='text-decoration:none'>" +
								"<i class='layui-icon'>&#xe640;</i>" +
							"</a>" +
							"</td>" +
							"</tr>";

		
	});
	$(".show_table>tbody").html($tr);
}

/**
 * 下架或上架商品
 * @param obj
 * @param id
 * @returns
 */
function product_out(obj,id) {
	if($(obj).attr("title") == "上架") {
		$.ajax({
  			type:"POST",
  			url:"/product/os-product/upProduct",
  			data:{productId:id},
  			dataType:"json",
  			success:function(data){
  				if(data == true) {
  					layer.msg('上架成功!',{icon:1,time:1000});
  					$(obj).parent().siblings().children(".showInShelve").text("已上架");
  					$(obj).children("i").html("<i class='layui-icon'>&#xe601;</i>");
  					$(obj).attr("title","下架");
  					$(obj).parent().siblings().children(".showInShelve").removeClass("layui-btn-danger");
  					$(obj).parent().siblings().children(".showInShelve").addClass("layui-btn-normal");
  				} else {
  					layer.msg('上架失败!',{icon:1,time:1000});
  				}
  			},
  			error:function() {
  				layer.msg('上架失败!',{icon:1,time:1000});
  			}
  		});
	} else {
		layer.confirm('确认要下架吗？',function(){
	        //发异步更新数据
	        $.ajax({
	  			type:"POST",
	  			url:"/product/os-product/outProduct",
	  			data:{productId:id},
	  			dataType:"json",
	  			success:function(data){
	  				if(data == true) {
	  					layer.msg('已下架!',{icon:1,time:1000});
	  					$(obj).parent().siblings().children(".showInShelve").text("已下架");
	  					$(obj).children("i").html("<i class='layui-icon'>&#xe608;</i>");
	  					$(obj).attr("title","上架");
	  					$(obj).parent().siblings().children(".showInShelve").removeClass("layui-btn-normal");
	  					$(obj).parent().siblings().children(".showInShelve").addClass("layui-btn-danger");
	  				} else {
	  					layer.msg('下架失败!',{icon:1,time:1000});
	  				}
	  			},
	  			error:function() {
	  				layer.msg('下架失败!',{icon:1,time:1000});
	  			}
	  		});      
	        return;
	    });
	}	
}

/**
 * 批量下架
 * @returns
 */
function products_out() {
	var $tr = $("input:checked:not(.allCheck)").parents("tr");
	var productIds = [];
	$tr.each(function(i,e) {
		
	})
}

var productInput = [];

function product_edit(obj) {
	
	var $td = $(obj).parent().parent().children(":gt(1):lt(5)");

	//添加保存取消按钮
	$("#save,#cancel").show();
	
	$td.each(function(i,e) {
		productInput[i] = $(e).text();
		$(e).html("<input class='layui-input editInput' type='text' value='"+$(e).text()+"' />");
	});
	
	var oneTrue = false;
	$('body').off('click').on('click', function(event) {
		if(!oneTrue){
			oneTrue=true;
			return;
		}
		
	    // IE支持 event.srcElement ， FF支持 event.target    
	    var evt = event.srcElement ? event.srcElement : event.target;    
	    if(evt.id == 'save') {
	    	$("#save,#cancel").hide();
	    	var labelName = $(".editInput").eq(1).val();
	    	
	    	var productId = $("#productId").text();
	    	var productNumber = $(".editInput").eq(0).val();
	    	var name = $(".editInput").eq(2).val();
	    	var showPrice =$(".editInput").eq(3).val();
	    	var shelveTime = $(".editInput").eq(4).val();
	    	var labelId = null;
	    	$.ajax({
	  			type:"POST",
	  			url:"/product/os-label/getLabelId",
	  			data:{name:labelName},
	  			dataType:"json",
	  			success:function(data){
	  				if(data != null) {
	  					labelId = data;
	  				}
	  				$.ajax({
	  		  			type:"POST",
	  		  			url:"/product/os-product/saveProduct",
	  		  			data:{productId:productId,productNumber:productNumber,labelId:labelId,name:name,showPrice:showPrice,shelveTime:shelveTime},
	  		  			dataType:"json",
	  		  			success:function(data){
	  		  				$(".editInput").each(function(i,e) {
	  		  					productInput[i] = $(e).val();
							});
		  		  			$td.each(function(i,e) {
		  			    		$(e).html(productInput[i]);
		  			    	});
	  		  				layer.msg('添加成功!',{icon:1,time:1000});
	  		  			}
	  		    	});
	  			}
	    	});
	    	
	    	
	    }
	    else if(evt.className == 'layui-input editInput'){ //如果元素是编辑
	        return;
	    } else {	//其他为取消
	    	$td.each(function(i,e) {
	    		$(e).html(productInput[i]);
	    	});
	    	$("#save,#cancel").hide();
	    }  
	});
}

/**
 * 删除商品
 * @param obj
 * @param id
 * @returns
 */
function product_del(obj,id) {
	$(obj).parents("tr").remove();
	layer.confirm('确认要删除吗？删除无法恢复',function(){
		$.ajax({
				type:"POST",
				url:"/product/os-product/delProduct",
				data:{productId:id},
				dataType:"json",
				success:function(data){
					if(data == true) {
						layer.msg('删除成功!',{icon:1,time:1000});
						//减少总数量
						$("#count").text($("#count").text()-1);
					}
						
					else 
						layer.msg('删除失败!',{icon:1,time:1000});
				},
				error:function() {
					layer.msg('删除失败!',{icon:1,time:1000});
				}
		});
	});
}

/*用户-添加*/
function product_add(title,url,w,h){
    x_admin_show(title,url,w,h);
}