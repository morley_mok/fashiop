layui.use(['laydate','laypage', 'layer'], function(){
  laydate = layui.laydate;//日期插件

  //以上模块根据需要引入
  //
  

        //批量删除提交
         function delAll () {
            layer.confirm('确认要删除吗？',function(index){
                //捉到所有被选中的，发异步进行删除
                layer.msg('删除成功', {icon: 1});
            });
         }
         /*用户-添加*/
        function member_add(title,url,w,h){
            x_admin_show(title,url,w,h);
        }
        /*用户-查看*/
        function member_show(title,url,id,w,h){
            x_admin_show(title,url,w,h);
        }

         /*用户-停用*/
        function member_stop(obj,id){
            layer.confirm('确认要停用吗？',function(index){
                //发异步把用户状态进行更改
                $(obj).parents("tr").find(".td-manage").prepend('<a style="text-decoration:none" onClick="member_start(this,id)" href="javascript:;" title="启用"><i class="layui-icon">&#xe62f;</i></a>');
                $(obj).parents("tr").find(".td-status").html('<span class="layui-btn layui-btn-disabled layui-btn-mini">已停用</span>');
                $(obj).remove();
                layer.msg('已停用!',{icon: 5,time:1000});
            });
        }

        /*用户-启用*/
        function member_start(obj,id){
            layer.confirm('确认要启用吗？',function(index){
                //发异步把用户状态进行更改
                $(obj).parents("tr").find(".td-manage").prepend('<a style="text-decoration:none" onClick="member_stop(this,id)" href="javascript:;" title="停用"><i class="layui-icon">&#xe601;</i></a>');
                $(obj).parents("tr").find(".td-status").html('<span class="layui-btn layui-btn-normal layui-btn-mini">已启用</span>');
                $(obj).remove();
                layer.msg('已启用!',{icon: 6,time:1000});
            });
        }
        // 用户-编辑
        function member_edit (title,url,id,w,h) {
            x_admin_show(title,url,w,h); 
        }
        /*密码-修改*/
        function member_password(title,url,id,w,h){
            x_admin_show(title,url,w,h);  
        }
        /*用户-删除*/
        function member_del(obj,id){
            layer.confirm('确认要删除吗？',function(index){
                //发异步删除数据
                $(obj).parents("tr").remove();
                layer.msg('已删除!',{icon:1,time:1000});
            });
        }
function getCount() {
	$.ajax({
		type:"POST",
		url:"/order/os-order/getCount",
		dataType:"json",
		success:function(data){
			return data;
		}
	});
}

var count = parseInt($("#count").text());


var laypage = layui.laypage,
	layer = layui.layer;
var pageCount = count%10==0?count/10:count/10+1;
layui.laypage({
    	cont: 'pages',
    	pages: pageCount,
    	jump:function(obj, first) {
    		var orderNumber =  $("[name=orderNumber]").val() ;
    		var username = $("[name=username]").val();
    		var orderstatus = $("[name=orderstatus]").val();
    		var current = obj.curr;
    		$.ajax({
    			type:"POST",
    			url:"/order/os-order/orderadmin",
    			data:{orderNumber:orderNumber,username:username,orderstatus:orderstatus,current:current},
    			dataType:"json",
				success:function(data){
					setList(data);
				}
			});	
    	}
});
			
$(".xbs").submit(function() {
	
	var orderNumber = $("[name=orderNumber]").val();
	var username=$("[name=consignee]").val();
	var orderstatus=$("#numbers option:selected").val();
	$.ajax({
		type:"POST",
		url:"/order/os-order/orderadmin",
		dataType:"json",
		data:{orderNumber:orderNumber,username:username,orderstatus:orderstatus},
	
		success:function(data){
			setList(data);
		
			$.ajax({
				type:"POST",
				url:"/order/os-order/getCount",
				dataType:"json",
				
				success:function(data){
					$("#count").text(data);
				
				
			    }
				
			});
		
		}	
	});
	
	return false;

});

function setList(data){
	var con="";
	var payType = "线下支付";
	var shipmentTime="不限送货时间";
	var shipmenttype="快递配送(免运费)";
	var orderStatus="未确认";
	$(data).each(function(index,item){
		if(item.payType == 1) {
			payType = "在线支付";
		}
		if(item.shipmentTime ==2){
			shipmentTime="工作日送货";
		}else if(item.shipmentTime ==3){
			shipmentTime="双休日、假日送货";
		}
		if(item.shipmenttype==1){
			shipmenttype="快递配送(运费)";
		}
		if(item.orderStatus==1){
			orderStatus="已确认";
		}else if(item.orderStatus==2){
			orderStatus="已配货";
		}else if(item.orderStatus==3){
			orderStatus="已发货";
		}else if(item.orderStatus==4){
			orderStatus="已收货";
		}else if(item.orderStatus==5){
			orderStatus="已完成";
		}
		con+="<tr>"+
		
		"<td>"+item.orderId+"</td>"+
		"<td>"+item.orderNumber+"</td>"+
		"<td>"+item.userId+"</td>"+
		"<td>"+
			"<div>"+
			"<p th:case=0>"+payType+"</p>"+
			"</div>"+
			"</td>"+
		"<td>"+
			"<div>"+
			"<p th:case=1>"+shipmentTime+"</p>"+
			"</div>"+
			"</td>"+
		"<td>"+"<div>"+
			"<p th:case=0>"+shipmenttype+"</p>"+
			"</div>"+
			"</td>"+
		"<td>"+item.shipmentAmount+"</td>"+
		"<td>"+item.invoiceTitle+"</td>"+
		"<td>"+"<div>"+
		"<p th:case=0>"+orderStatus+"</p>"+
		"</div>"+
		"</td>"+
		"<td>"+item.orderAmount+"</td>"+
		"<td>"+item.orderScore+"</td>"+
		"<td>"+item.payAmount+"</td>"+
		"<td>"+item.buyNumber+"</td>"+
		"</tr>";
	});
	console.log(con);
	$("#ordertbody").html(con);
}
});

 