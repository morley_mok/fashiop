$(function() {
	//获取验证码
	$("#getCode").click(function() {
		var telephone = $("[name=telephone]").val();
		if(telephone == "") {
			return;
		}
		$(this).attr("disabled",true);
		
		$.ajax({
			type:"POST",
			url:"/user/os-user/getRegCode",
			data:{telephone:telephone},
			dataType:"json",
			success:function(data){
				if(data != "OK") {
					alert("获取失败!");
				}
			},
			error:function(XMLHttpRequest, textStatus, errorThrown){
				alert(XMLHttpRequest);
				alert(textStatus);
				alert(errorThrown);
			}
		});
		
		countDown(60);
	});
	
	$(".reg_form").submit(function() {
		if(nonEmpty() == false) {
			return false;
		}
		
		var userName = $("[name=userName]").val();
		var email = $("[name=email]").val();
		var loginPassword = $("[name=loginPassword]").val();
		var telephone = $("[name=telephone]").val();
		
		$.ajax({
			type:"POST",
			url:"/user/os-user/registerDispose",
			data:{userName:userName,email:email,loginPassword:loginPassword,telephone:telephone},
			dataType:"json",
			success:function(data){
				alert("register OK!");
			}
		});
		return false;
	});
});

//倒计时
function countDown(time) {
	$("#getCode").val(time + "秒后重试");
	if(time == 0) {
		$("#getCode").attr("disabled",false).val("获取验证码");
		return;
	}
	time --;
	timerID = setTimeout("countDown("+time+")",1000);
}

//非空验证
function nonEmpty() {
	var bool = false;
	var userName = $("[name=userName]").val();
	var loginPassword = $("[name=loginPassword]").val();
	var regPass = $("[name=repass]").val();
	var telephone = $("[name=telephone]").val();
	if(userName == "") {
		alert("用户名不能为空");
	} else if(loginPassword == "") {
		alert("密码不能为空");
	} else if(loginPassword != regPass) {
		alert("两次密码输入不一致");
	} else if(telephone == "") {
		alert("手机号码不能为空");
	} else {
		var verificationCode = $("[name=verificationCode]").val();
		if(verificationCode == "") {
			alert("没有输入验证码");
			return bool;
		}
		//判断验证码是否正确
		$.ajax({
			type:"POST",
			url:"/user/os-user/verification",
			data:{code:verificationCode},
			dataType:"json",
			success:function(data){
				if(data == false){
					alert("验证码错误！");
				} else {
					bool = true;
				}
			},
			error:function() {
			}
		});
	}
	return bool;
	
}