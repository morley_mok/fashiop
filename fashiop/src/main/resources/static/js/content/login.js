$(function() {
	//获取验证码
	$("#getCode").click(function() {
		var telephone = $("[name=telephone]").val();
		if(telephone == "") {
			return;
		}
		$(this).attr("disabled",true);
		
		$.ajax({
			type:"POST",
			url:"/user/os-user/getCode",
			data:{telephone:telephone},
			dataType:"json",
			success:function(data){
				if(data != "OK") {
					alert("获取失败!");
				}
			},
			error:function(XMLHttpRequest, textStatus, errorThrown){
				alert(XMLHttpRequest);
				alert(textStatus);
				alert(errorThrown);
			}
		});
		
		countDown(60);
	});
	
	$(".login_form").submit(function() {
		//var bool = nonEmpty();
		if(nonEmpty() == false) {
			return false;
		}
		var userName = $("[name=userName]").val();
		var loginPassword = $("[name=loginPassword]").val();
		var telephone = $("[name=telephone]").val();
		
		$.ajax({
			type:"POST",
			url:"/user/os-user/loginDispose",
			data:{userName:userName,loginPassword:loginPassword,telephone:telephone},
			dataType:"json",
			success:function(data){
				alert("hello");
			}
		});
		return false;
	});
	
	$(".other_qq").click(function() {
		alert(123);
	});
});

//倒计时
function countDown(time) {
	$("#getCode").val(time + "秒后重试");
	if(time == 0) {
		$("#getCode").attr("disabled",false).val("获取验证码");
		return;
	}
	time --;
	timerID = setTimeout("countDown("+time+")",1000);
}

//非空验证
function nonEmpty() {
	var bool = false;
	var telephone = $("[name=telephone]").val();
	if($("[name=userName]").val() == "" || $("[name=loginPassword]").val() == "" || telephone == "") {
		alert("账号/密码/手机号不能为空");
	}
	
	var verificationCode = $("[name=verificationCode]").val();
	if(verificationCode == "") {
		alert("没有输入验证码");
	}
	//判断验证码是否正确
	$.ajax({
		type:"POST",
		url:"/user/os-user/verification",
		data:{code:verificationCode},
		dataType:"json",
		success:function(data){
			if(data == false){
				alert("验证码错误！");
			} else {
				bool = true;
			}
		},
		error:function() {
		}
	});
	
	return bool;
	
}