function selprocategroy() {
			/* 展示商品类型 */
			/*$.get("http://localhost:8080/product/os-category/selprocategroy",{},function(data){
				
			})*/
		}
		
		var count;//总数据量
		var countpage;//总页数
		var regEx = /^([1-9]\d*|[0]{1,1})$/;  //验证非负整数（正整数 + 0）
		function getprocount() {
			$.ajax({
				url:"http://localhost:8080/product/os-product/getprocount",
				data:{},
				async:false,
				success:function(data){
					count=data;
				}
			});
		}
		
		/* 得到总页数 */
		function getcountpage(pages) {
			countpage=Math.ceil(count/pages);
		}
		function showInfo(current,pages,sort,cname) {
			/* 展示商品数据 */
			$.get("http://localhost:8080/product/os-category/showcategory",{"current":current,"pages":pages,"sort":sort,"cname":cname},function(data){
				var html="";
				$(data).each(function(i){
					html+="<div class='col-lg-3 col-md-3 col-sm-6'>";
					html+="<div class='f_p_item'>";
					html+="<div class='f_p_img'>";
					html+="<img onclick='proinfoshow("+data[i].product_id+")' class='img-fluid' src='"+data[i].pic_img+"' alt=''>";
					html+="<div class='p_icon'>";
					html+="<a onclick='lnrheart("+data[i].product_id+")' href='#'><i class='lnr lnr-heart'></i></a>";
					html+="<a onclick='lnrcart("+data[i].product_id+")' href='#'><i class='lnr lnr-cart'></i></a>";
					html+="</div>";
					html+="</div>";
					html+="<a onclick='proinfoshow("+data[i].product_id+")' href=''><h4>"+data[i].name+"</h4></a>";
					html+="<h5>￥"+data[i].show_price+"</h5>";
					html+="</div>";
					html+="</div>";
				});
				$(".latest_product_inner").html(html);
			})
		}
		
		$(function() {
			/* 得到商品总数 */
			getprocount();
			/* 分类浏览展示 */
			selprocategroy();
			
			/* 页面加载展示数据 */
			showInfo();
			
			/* 商品排序 1  默认  2.发布时间>  3销量<  4.价格< */
			$(".sorting .option").click(function() {
				var current = $(".pagination .active").find("a").html();
				var sort = $(this).attr("data-value");
				var pages = $(".show .selected").attr("data-value");
				showInfo(current,pages,sort);
			});
			
			/* 展示数量 8,12,16*/
			$(".show .option").click(function() {
				var current = $(".pagination .active").find("a").html();
				var sort = $(".sorting .selected").attr("data-value");
				var pages = $(this).attr("data-value");
				showInfo(current,pages,sort);
			});
			
			/* 分页点击 */
			$(".page-item").click(function(){
				var current = $(this).find("a").html();
				var pages = $(".show .selected").attr("data-value");
				var sort = $(".sorting .selected").attr("data-value");
				if(regEx.test(current)){
					getcountpage(pages);//得到总页数
					if(current>countpage){
						layer.msg("最大页不超过"+current+"页");
					}else{
						$(this).addClass("active").siblings().removeClass("active");
						showInfo(current,pages,sort);
					}
				}
			});
			
			$(".fa-long-arrow-left").parent().click(function(){
				var current =$(".pagination .active").find("a").html();
				var sort = $(".sorting .selected").attr("data-value");
				var pages = $(".show .selected").attr("data-value");
				var one = $(this).parent().next().find("a").html();
				getcountpage(pages);//得到总页数
				if(Number(current)<=1){
					layer.msg("当前已经是第一页");
				}else if(one>1){
					$(".pagination .active").find("a").html(Number(current)-1);
					$(".pagination .active").prev().find("a").html(Number(current)-2);
					$(".pagination .active").prev().prev().find("a").html(Number(current)-3);
					showInfo(Number(current)-1,pages,sort);
				}else{
					$(".pagination .active").prev().addClass("active").siblings().removeClass("active");
					showInfo(Number(current)-1,pages,sort);
				}
			});
			
			$(".fa-long-arrow-right").parent().click(function(){
				var current =$(".pagination .active").find("a").html();
				var sort = $(".sorting .selected").attr("data-value");
				var pages = $(".show .selected").attr("data-value");
				getcountpage(pages);//得到总页数
				if(current>0 && current<3 && current<countpage){
					$(".pagination .active").next().addClass("active").siblings().removeClass("active");
					showInfo(Number(current)+1,pages,sort);
				}else if(current<countpage){
					$(".pagination .active").find("a").html(Number(current)+1);
					$(".pagination .active").prev().find("a").html(Number(current));
					$(".pagination .active").prev().prev().find("a").html(Number(current)-1);
					showInfo(Number(current)+1,pages,sort);
				}else{
					layer.msg("当前已经是最后一页");
				}
			});
		});
		
		/* 商品详情页展示 */
		function proinfoshow(proid){
			$(location).attr("href","http://localhost:8080/product/os-product/prodetailshow?productId="+proid);
		}
		
		/**
		 * 用户收藏商品
		 */
		function lnrheart(proid){
			var userId = $("#userid").val();
			if(userId==-100){
				layer.msg("请先登录"+userid+"  "+proid);
				return false;
			}
			//询问框
			layer.confirm("您确定要收藏该商品么",
					{btn:["确定","不了"]},function(){
						$.ajax({
							url:"http://localhost:8080/user/os-favorite/wishlist",
							data:{"userId":userId,"productId":proid},
							success:function(data){
								if("success"==data){
									layer.msg("商品收藏成功",{time:1000,icon:6});
								}else{
									layer.msg("该商品已经收藏",{time:1000,icon:4});
								}
							}
						});
					}
			);
		}
		
		/**
		 *商品加入购物车
		 */
		function lnrcart(proid){
			//询问框
			layer.confirm("是否将该商品加入购物车",
					{btn:["是的","不了"]},function(){
						$.get("http://localhost:8080/product/os-shopping-cart/addcart",{"productId":proid},function(data){
						if(data==1){
							layer.msg("商品成功加入购物车	",{icon:6});
						}else if(data==2){
							layer.msg("商品购物车数量+1",{icon:1});
						}
					})
					}
			);
		}
