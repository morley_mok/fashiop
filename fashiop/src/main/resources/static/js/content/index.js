/**
 * 前台商品详细详细展示
 */
function showInHot(current){
	if(current==null){
		current=1;
	}
	var html="";
	$.ajax({
		url:"/product/os-product/showProductHot",
		data:{"current":current},
		dataType:"json",
		success:function(page){
			$.each(page.records,function(i,e){
				html+="<div class='col col"+(i+1)+"'>";
				html+="<div class='f_p_item'>";
				html+="<div class='f_p_img'>";
				html+="<img onclick='proinfoshow("+e.productId+")' class='img-fluid' src='"+e.picImg+"' alt=''>";
				//图片需要先修改数据库src下替换为e.picImg   /img/product/feature-product/f-p-4.jpg
				html+="<div class='p_icon'>";
				html+="<a onclick='lnrheart("+e.productId+")' href='javascript:void(0)'><i class='lnr lnr-heart'></i></a>";
				html+="<a onclick='lnrcart("+e.productId+")' href='javascript:void(0)'><i class='lnr lnr-cart'></i></a>";
				html+="</div>";
				html+="</div>";
				html+="<a onclick='proinfoshow("+e.productId+")' href='javascript:void(0)'><h4>"+e.name+"</h4></a>";
				html+="<h5>￥"+e.showPrice+"</h5>";
				html+="</div>";
				html+="</div>";
				//alert("索引:"+i+"对应值为："+e.productId);
			})
			//总页数保存在input中
			$("#pages").attr("value",Number(page.pages));
			$("#product_hot").html(html);
		}
	});
}

$(function() {
	showInHot();
	/* 页码分页 */
	$(".page-link").click(function() {
		var regEx = /^([1-9]\d*|[0]{1,1})$/;  //验证非负整数（正整数 + 0）
		var current = Number($(this).html());
		if(regEx.test(current)){
			$(this).parent().addClass("active").siblings().removeClass("active");
			showInHot(current);
		}
	});
	//上一页
	$(".page-link:first").click(function(){
		var current = Number($(".active>span").html());
		if(current>1&&current<4){
			$(".active").prev().addClass("active").siblings().removeClass("active");
			showInHot(current-1);
		}
		if(current>3){
			var current = Number($(".active>span").html());
			$(".active>span").html("0"+Number(current-1));
			$(".active").prev().find("span").html("0"+Number(current-2));
			$(".active").prev().prev().find("span").html("0"+Number(current-3));
			showInHot(current-1);
		}
		if(current==1){
			layer.msg("当前已经是第一页");
		}
	});
	//下一页
	$(".page-link:last").click(function(){
		var current = Number($(".active>span").html());
		if(current>0&&current<3){
			$(".active").next().addClass("active").siblings().removeClass("active");
			showInHot(current+1);
		}
		var pages = Number($("#pages").val());
		if(current>2&&current<pages){
			var current = Number($(".active>span").html());
			$(".active>span").html("0"+Number(current+1));
			$(".active").prev().find("span").html("0"+Number(current));
			$(".active").prev().prev().find("span").html("0"+Number(current-1));
			showInHot(current+1);
		}
		if(current==pages){
			layer.msg("当前已经是最后一页");
		}
	});
});

/**
 * 用户收藏商品
 */
function lnrheart(proid){
	var userId = $("#userid").val();
	if(userId==-100){
		layer.msg("请先登录"+userid+"  "+proid);
		return false;
	}
	//询问框
	layer.confirm("您确定要收藏该商品么",
			{btn:["确定","不了"]},function(){
				$.ajax({
					url:"http://localhost:8080/user/os-favorite/wishlist",
					data:{"userId":userId,"productId":proid},
					success:function(data){
						if("success"==data){
							layer.msg("商品收藏成功",{time:1000,icon:6});
						}else{
							layer.msg("该商品已经收藏",{time:1000,icon:4});
						}
					}
				});
			}
	);
}

/**
 *商品加入购物车
 */
function lnrcart(proid){
	//询问框
	layer.confirm("是否将该商品加入购物车",
			{btn:["是的","不了"]},function(){
				$.get("http://localhost:8080/product/os-shopping-cart/addcart",{"productId":proid},function(data){
				if(data==1){
					layer.msg("商品成功加入购物车	",{icon:6});
				}else if(data==2){
					layer.msg("商品购物车数量+1",{icon:1});
				}
			})
			}
	);
}

