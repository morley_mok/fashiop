package top.shenyuboos.fashiop.user.service.impl;

import top.shenyuboos.fashiop.system.entity.CmsRole;
import top.shenyuboos.fashiop.user.entity.OsFavorite;
import top.shenyuboos.fashiop.user.entity.OsUser;
import top.shenyuboos.fashiop.user.mapper.OsUserMapper;
import top.shenyuboos.fashiop.user.service.IOsUserService;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Service
public class OsUserServiceImpl extends ServiceImpl<OsUserMapper, OsUser> implements IOsUserService {

	@Resource
	private OsUserMapper osUserMapper;
	
	@Override
	public OsUser findUserByName(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CmsRole getRole(Long userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<OsUser> getUserAll() {
		// TODO Auto-generated method stub
		return osUserMapper.getUserAll();
	}

	@Override
	public List<OsUser> getAllUser() {
		return osUserMapper.getAllUser();
	}

}
