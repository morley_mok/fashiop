package top.shenyuboos.fashiop.user.service;

import top.shenyuboos.fashiop.user.entity.CmsUser;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 管理员表 服务类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface ICmsUserService extends IService<CmsUser> {

	CmsUser findUserByName(String username);
	
	
}
