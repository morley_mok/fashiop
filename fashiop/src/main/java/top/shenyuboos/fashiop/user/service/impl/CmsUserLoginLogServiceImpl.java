package top.shenyuboos.fashiop.user.service.impl;

import top.shenyuboos.fashiop.user.entity.CmsUser;
import top.shenyuboos.fashiop.user.entity.CmsUserLoginLog;
import top.shenyuboos.fashiop.user.mapper.CmsUserLoginLogMapper;
import top.shenyuboos.fashiop.user.service.ICmsUserLoginLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 管理员登陆表 服务实现类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Service
public class CmsUserLoginLogServiceImpl extends ServiceImpl<CmsUserLoginLogMapper, CmsUserLoginLog> implements ICmsUserLoginLogService {

	@Resource
	private CmsUserLoginLogMapper cmsUserLoginLogMapper;
	
	/* 
	 *后台登录实现
	 */
	@Override
	public CmsUser cmslogin(CmsUser cmsUser) {
		return cmsUserLoginLogMapper.CmsUserLogin(cmsUser);
	}
	
	
	
}
