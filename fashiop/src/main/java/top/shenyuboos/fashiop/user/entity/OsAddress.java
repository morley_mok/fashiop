package top.shenyuboos.fashiop.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 收获地址表
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class OsAddress implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 收获地址ID
     */
    @TableId(value = "address_id", type = IdType.AUTO)
    private Long addressId;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 姓名
     */
    private String userName;

    /**
     * 地址标签
     */
    private String userTag;

    /**
     * 手机号码
     */
    private String userPhone;

    /**
     * 省份ID
     */
    private Integer provinceId;

    /**
     * 省份名字
     */
    private String provinceName;

    /**
     * 城市ID
     */
    private Integer cityId;

    /**
     * 城市名字
     */
    private String cityName;

    /**
     * 区域ID
     */
    private Integer districtId;

    /**
     * 区域名字
     */
    private String districtName;

    /**
     * 详细地址
     */
    private String userAdress;

    /**
     * 邮政编码
     */
    private Integer userZipcode;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

	@Override
	public String toString() {
		return "OsAddress [addressId=" + addressId + ", userId=" + userId + ", userName=" + userName + ", userTag="
				+ userTag + ", userPhone=" + userPhone + ", provinceId=" + provinceId + ", provinceName=" + provinceName
				+ ", cityId=" + cityId + ", cityName=" + cityName + ", districtId=" + districtId + ", districtName="
				+ districtName + ", userAdress=" + userAdress + ", userZipcode=" + userZipcode + ", createTime="
				+ createTime + ", updateTime=" + updateTime + "]";
	}
    
    

}
