package top.shenyuboos.fashiop.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 管理员表
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class CmsUser implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 管理员ID
     */
    @TableId(value = "user_id", type = IdType.AUTO)
    private Long userId;

    /**
     * 组织ID
     */
    private Long organizationId;

    /**
     * 管理员账号
     */
    private String loginName;

    /**
     * 管理员密码
     */
    private String loginPassword;

    /**
     * 加密密码的盐
     */
    private String salt;

    /**
     * 昵称
     */
    private String userName;

    /**
     * 真实姓名
     */
    private String realName;

    /**
     * 性别 0=保密/1=男/2=女
     */
    private Integer sex;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 用户头像
     */
    private String picImg;

    /**
     * 状态 0=冻结/1=正常
     */
    private Integer status;

    /**
     * 电子邮箱
     */
    private String email;

    /**
     * 手机号码
     */
    private String telephone;

    /**
     * 登录方式 0=系统登录/1=QQ登录/2微信登录/3=其他
     */
    private Integer loginType;

    /**
     * 最后登录时间
     */
    private LocalDateTime lastLoginTime;

    /**
     * 最后登录IP
     */
    private String lastLoginIp;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 创建者
     */
    private String createBy;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 更新者
     */
    private String updateBy;

	@Override
	public String toString() {
		return "CmsUser [userId=" + userId + ", organizationId=" + organizationId + ", loginName=" + loginName
				+ ", loginPassword=" + loginPassword + ", salt=" + salt + ", userName=" + userName + ", realName="
				+ realName + ", sex=" + sex + ", age=" + age + ", picImg=" + picImg + ", status=" + status + ", email="
				+ email + ", telephone=" + telephone + ", loginType=" + loginType + ", lastLoginTime=" + lastLoginTime
				+ ", lastLoginIp=" + lastLoginIp + ", createTime=" + createTime + ", createBy=" + createBy
				+ ", updateTime=" + updateTime + ", updateBy=" + updateBy + "]";
	}

    
}
