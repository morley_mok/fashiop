package top.shenyuboos.fashiop.user.service;

import top.shenyuboos.fashiop.user.entity.OsAddress;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 收获地址表 服务类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-30
 */
public interface IOsAddressService extends IService<OsAddress> {

}
