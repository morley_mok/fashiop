package top.shenyuboos.fashiop.user.controller;


import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import top.shenyuboos.fashiop.user.entity.OsFavorite;
import top.shenyuboos.fashiop.user.service.IOsFavoriteService;

/**
 * <p>
 * 收藏夹表 前端控制器
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-12-02
 */
@Controller
@RequestMapping("/user/os-favorite")
public class OsFavoriteController {
	
	Logger logger = Logger.getLogger(OsFavoriteController.class);
	
	@Resource
	private IOsFavoriteService osFavoriteServiceImpl;
	
	/**
	 * 商品收藏
	 * @throws Exception 
	 */
	@RequestMapping("wishlist")
	@ResponseBody
	public Object Wishlist(OsFavorite osFavorite) throws Exception {
		boolean favorite = osFavoriteServiceImpl.isFavorite(osFavorite);
		if(favorite) {
			return "collection";
		}else {
			try {
				osFavoriteServiceImpl.userAddProFavorite(osFavorite);
				return "success";
			} catch (Exception e) {
				return "error";
			}
		}
	}
	
	@RequestMapping("osfavorite")
	@ResponseBody
	public Object osFavorite(Integer current,String time,String userName){
		String name="";
		Date DateTime = null;
		if(!time.equals("")) {
			try {
				DateTime = new SimpleDateFormat("yyyy-MM-dd").parse(time);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		if(userName!=null&&!userName.equals("")) {
			name = userName;
		}
		Page<OsFavorite> page = new Page<OsFavorite>();
		page.setCurrent(current);
		page.setSize(1);
		page.setRecords(osFavoriteServiceImpl.getFavoriteAll(page,DateTime, userName));
		return page;
	}
}
