package top.shenyuboos.fashiop.user.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class OsUser implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * 用户ID
     */
    @TableId(value = "user_id", type = IdType.AUTO)
    private Long userId;

    /**	
     * 用户编号
     */
    private Long userNumber;

    /**
     * 昵称
     */
    private String userName;

    /**
     * 登录密码
     */
    private String loginPassword;

    /**
     * 加密密码的盐
     */
    private String salt;

    /**
     * 真实姓名
     */
    private String realName;

    /**
     * 性别 0=保密/1=男/2=女
     */
    private Integer sex;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 用户头像
     */
    private String picImg;

    /**
     * 状态 0=冻结/1=正常
     */
    private Boolean status;

    /**
     * 邮箱激活 0=未激活/1=已激活
     */
    private Boolean emailIsActive;

    /**
     * 电子邮箱
     */
    private String email;

    /**
     * 手机号码
     */
    private String telephone;

    /**
     * 最后登录时间
     */
    private LocalDateTime lastLoginTime;

    /**
     * 最后登录IP
     */
    private String lastLoginIp;

    /**
     * 登录次数
     */
    private Long loginNumber;

    /**
     * 注册时间
     */
    private LocalDateTime regeistTime;

    /**
     * 创建者
     */
    private String createBy;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 更新者
     */
    private String updateBy;

    /**
     * 消费额
     */
    private BigDecimal amount;

    /**
     * 会员等级ID
     */
    private Long rankId;

    /**
     * 会员积分
     */
    private Integer score;
    
    private String userAdress;

	@Override
	public String toString() {
		return "OsUser [userId=" + userId + ", userNumber=" + userNumber + ", userName=" + userName + ", loginPassword="
				+ loginPassword + ", salt=" + salt + ", realName=" + realName + ", sex=" + sex + ", age=" + age
				+ ", picImg=" + picImg + ", status=" + status + ", emailIsActive=" + emailIsActive + ", email=" + email
				+ ", telephone=" + telephone + ", lastLoginTime=" + lastLoginTime + ", lastLoginIp=" + lastLoginIp
				+ ", loginNumber=" + loginNumber + ", regeistTime=" + regeistTime + ", createBy=" + createBy
				+ ", updateTime=" + updateTime + ", updateBy=" + updateBy + ", amount=" + amount + ", rankId=" + rankId
				+ ", score=" + score + ", userAdress=" + userAdress + "]";
	}
    

	
    

}
