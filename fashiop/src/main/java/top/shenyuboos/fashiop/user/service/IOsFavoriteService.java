package top.shenyuboos.fashiop.user.service;

import top.shenyuboos.fashiop.user.entity.OsFavorite;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 收藏夹表 服务类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-12-02
 */
public interface IOsFavoriteService extends IService<OsFavorite> {
	
	
	/**
	 * 用户添加喜爱商品，商品收藏
	 */
	int userAddProFavorite(OsFavorite osFavorite);
	
	/**
	 * 判断商品是否已经收藏
	 */
	boolean isFavorite(OsFavorite osFavorite);
	
	/*======后台=======*/
	/**
	 * 
	 */
	List<OsFavorite> getFavoriteAll(Page<OsFavorite> page,Date time,String userName);
	
	
}
