package top.shenyuboos.fashiop.user.mapper;

import top.shenyuboos.fashiop.user.entity.OsUserLoginLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户登录表 Mapper 接口
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface OsUserLoginLogMapper extends BaseMapper<OsUserLoginLog> {

}
