package top.shenyuboos.fashiop.user.service;

import top.shenyuboos.fashiop.user.entity.OsUserLoginLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户登录表 服务类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface IOsUserLoginLogService extends IService<OsUserLoginLog> {

}
