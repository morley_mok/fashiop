package top.shenyuboos.fashiop.user.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 用户登录表 前端控制器
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@RestController
@RequestMapping("/user/os-user-login-log")
public class OsUserLoginLogController {
	
}
