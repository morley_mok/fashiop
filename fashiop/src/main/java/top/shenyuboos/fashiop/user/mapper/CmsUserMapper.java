package top.shenyuboos.fashiop.user.mapper;

import top.shenyuboos.fashiop.user.entity.CmsUser;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 管理员表 Mapper 接口
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface CmsUserMapper extends BaseMapper<CmsUser> {
	
	

	
}
