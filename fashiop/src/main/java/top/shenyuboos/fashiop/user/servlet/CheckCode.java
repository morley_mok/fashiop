package top.shenyuboos.fashiop.user.servlet;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 图片验证码
 * 
 * @author JoelJhou
 * 
 */
@WebServlet("/checkcode")
public class CheckCode extends HttpServlet {
	private static final long serialVersionUID = 1532787236715859525L;
	/**
	 * 验证图片宽
	 */
	private static final int WIDTH = 80;

	/**
	 * 验证图片高度
	 */
	private static final int HEIGHT = 20;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 获得图片
		BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
		// 获取画笔
		Graphics g = image.getGraphics();

		// 设置边框
		setBounds(g);

		// // 设置背景
		// setBackGround(g);

		// 干扰线
		drawRandomLine(g);

		// 验证码
		drawRandomString((Graphics2D) g, request);

		// 发送给页面
		response.setContentType("image/jpeg");
		response.setDateHeader("expries", -1);
		response.setHeader("cache-control", "no-cache");
		response.setHeader("progma", "no-cache");
		ImageIO.write(image, "jpg", response.getOutputStream());
	}

	/**
	 * 边框
	 * 
	 * @param g
	 */
	private void setBounds(Graphics g) {

		g.setColor(Color.BLUE);
		g.fillRect(0, 0, WIDTH, HEIGHT);

		g.setColor(Color.WHITE);
		g.fillRect(1, 1, WIDTH - 2, HEIGHT - 2);

	}

	/**
	 * 背景
	 * 
	 * @param g
	 */
	// private void setBackGround(Graphics g) {
	//
	// g.setColor(Color.WHITE);
	// g.drawRect(1, 1, WIDTH-2, HEIGHT-2);
	//
	// }

	/**
	 * 干扰线
	 * 
	 * @param g
	 */
	private void drawRandomLine(Graphics g) {

		g.setColor(Color.GREEN);

		int x1, y1;
		int x2, y2;

		Random r = new Random();

		for (int i = 0; i < 5; i++) {
			// 起始xy
			x1 = r.nextInt(WIDTH);
			y1 = r.nextInt(HEIGHT);
			// 结束xy
			x2 = r.nextInt(WIDTH);
			y2 = r.nextInt(HEIGHT);

			g.drawLine(x1, y1, x2, y2);
		}

	}

	/**
	 * 验证码
	 * 
	 * @param g
	 */
	private void drawRandomString(Graphics2D g, HttpServletRequest request) {

		g.setColor(Color.RED);
		g.setFont(new Font("宋体", Font.BOLD, 20));
		String base = "qwertyupasdfghjkzxcvbnm23456789QWERTYUIOPASDFGHJKLZXCVBNM";
		StringBuilder sb = new StringBuilder();
		char ch = '\u0000';
		int degree = 0;
		Random r = new Random();
		int startX = 8;

		for (int i = 0; i < 4; i++) {
			ch = base.charAt(r.nextInt(base.length()));

			// 设置旋转, ±20°
			degree = r.nextInt() % 20;
			g.rotate(degree * Math.PI / 180, startX, 14);
			g.drawString(ch + "", startX, 16);
			// 取消旋转
			g.rotate(-degree * Math.PI / 180, startX, 14);
			
			startX += 18;
			sb.append(ch);
		}

		//保存到session中
		request.getSession().setAttribute("checkCode", sb.toString().toLowerCase());
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
