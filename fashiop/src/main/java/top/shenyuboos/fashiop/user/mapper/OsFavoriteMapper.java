package top.shenyuboos.fashiop.user.mapper;

import top.shenyuboos.fashiop.user.entity.OsFavorite;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * <p>
 * 收藏夹表 Mapper 接口
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-12-02
 */
public interface OsFavoriteMapper extends BaseMapper<OsFavorite> {
	
	//商品收藏
	
	//判断商品是否已经收藏
	@Select("SELECT COUNT(1) FROM `os_favorite` WHERE `user_id`=#{userId} AND `product_id`=#{productId}")
	int isFavorite(OsFavorite osFavorite);
	
	
	/**
	 *根据时间和用户名查询商品 收藏
	 *
	 */
	List<OsFavorite> getFavoriteAll(Page<OsFavorite> page,@Param("time")Date time,@Param("userName")String userName);
}
