package top.shenyuboos.fashiop.user.service.impl;

import top.shenyuboos.fashiop.user.entity.OsUserLoginLog;
import top.shenyuboos.fashiop.user.mapper.OsUserLoginLogMapper;
import top.shenyuboos.fashiop.user.service.IOsUserLoginLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户登录表 服务实现类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Service
public class OsUserLoginLogServiceImpl extends ServiceImpl<OsUserLoginLogMapper, OsUserLoginLog> implements IOsUserLoginLogService {

}
