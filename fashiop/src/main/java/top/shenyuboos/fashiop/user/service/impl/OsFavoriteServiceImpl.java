package top.shenyuboos.fashiop.user.service.impl;

import top.shenyuboos.fashiop.user.entity.OsFavorite;
import top.shenyuboos.fashiop.user.mapper.OsFavoriteMapper;
import top.shenyuboos.fashiop.user.service.IOsFavoriteService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 收藏夹表 服务实现类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-12-02
 */
@Service
public class OsFavoriteServiceImpl extends ServiceImpl<OsFavoriteMapper, OsFavorite> implements IOsFavoriteService {

	@Resource
	private OsFavoriteMapper osFavoriteMapper;
	
	/**
	 * 商品收藏
	 */
	@Override
	public int userAddProFavorite(OsFavorite osFavorite) {
		return osFavoriteMapper.insert(osFavorite);
	}

	/**
	 * 判断商品是否已经收藏
	 */
	@Override
	public boolean isFavorite(OsFavorite osFavorite) {
		return osFavoriteMapper.isFavorite(osFavorite)>0;//true:已经收藏
	}

	@Override
	public List<OsFavorite> getFavoriteAll(Page<OsFavorite> page,Date time, String userName) {
		return osFavoriteMapper.getFavoriteAll(page,time, userName);
	}

}
