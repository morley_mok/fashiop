package top.shenyuboos.fashiop.user.service;

import top.shenyuboos.fashiop.user.entity.CmsUser;
import top.shenyuboos.fashiop.user.entity.CmsUserLoginLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 管理员登陆表 服务类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface ICmsUserLoginLogService extends IService<CmsUserLoginLog> {

	int CMSLOGIN_CHECKCODE = 1;// 验证码错误
	int CMSLOGIN_SUCCESS = 2;// 登录成功
	int CMSLOGIN_ERROE = 3;// 登录异常

	/**
	 * 后台登录
	 */
	CmsUser cmslogin(CmsUser cmsUser);
}
