package top.shenyuboos.fashiop.user.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.alibaba.fastjson.JSON;

import top.shenyuboos.fashiop.user.entity.OsFavorite;
import top.shenyuboos.fashiop.user.entity.OsUser;
import top.shenyuboos.fashiop.user.service.IOsUserService;
import top.shenyuboos.fashiop.utils.Sms;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Controller
@RequestMapping({ "/user/os-user", "/" })
public class OsUserController {

	Logger logger = Logger.getLogger(OsUserController.class);
	
	@Resource
	private IOsUserService iOsUserService;
	
	private static final Logger log = Logger.getLogger(OsUserController.class);

	@RequestMapping("index")
	public String index() {
		
		return "index";
	}
	
	@RequestMapping("login")
	public String login() {
		return "login";
	}
	
	@RequestMapping("register")
	public String register() {
		return "registration";
	}

	/**
	 * 登录
	 * 
	 * @param user
	 * @return
	 */
	@RequestMapping("loginDispose")
	@ResponseBody
	public String loginDispose(OsUser user) {
		log.info(user);
		return JSON.toJSONString(user);
	}
	
	/**
	 * 用户id获取
	 */
	@ResponseBody
	@RequestMapping("getuserid")
	public String getuserid(HttpServletRequest request) {
		return request.getSession().getAttribute("userid")==null?"-100":request.getSession().getAttribute("userid").toString();
	}
	
	/**
	 * 获取登录短信验证码
	 * 
	 * @return
	 */
	@RequestMapping("getCode")
	@ResponseBody
	public String getCode(String telephone,HttpSession session) {
		Random random = new Random();
		StringBuffer code = new StringBuffer();
		for (int i = 0; i < 6; i++) {
			code.append(random.nextInt(10));
		}
		session.setAttribute("code", code.toString());
		SendSmsResponse ssr = null;
		try {
			ssr = Sms.sendSms(telephone, code.toString(),"SMS_149422564");
		} catch (ClientException e) {
			e.printStackTrace();
		}
		return ssr.getCode();
	}
	
	/**
	 * 获取注册短信验证码
	 * 
	 * @return
	 */
	@RequestMapping("getRegCode")
	@ResponseBody
	public String getRegCode(String telephone,HttpSession session) {
		Random random = new Random();
		StringBuffer code = new StringBuffer();
		for (int i = 0; i < 6; i++) {
			code.append(random.nextInt(10));
		}
		session.setAttribute("code", code.toString());
		SendSmsResponse ssr = null;
		try {
			ssr = Sms.sendSms(telephone, code.toString(),"SMS_151578981");
		} catch (ClientException e) {
			e.printStackTrace();
		}
		return ssr.getCode();
	}
	
	/**
	 * 验证短信验证码是否正确
	 * @return
	 */
	@RequestMapping("verification")
	@ResponseBody
	public String verification(String code,HttpSession session) {
		if (code == null) {
			return "false";
		}
		if (code.equals(session.getAttribute(code))) {
			return "true";
		}
		return "false";
	}	
	
	@RequestMapping("cmsuserlist")
	@ResponseBody
	public List<OsUser> userList() {
		System.err.println("执行CmsUserController");
		return iOsUserService.getUserAll();
	}
	
	
	@RequestMapping("userlist")
	public String userlist(Model model){
		List<OsUser> users = iOsUserService.getAllUser();
		for (int i = 0; i < users.size(); i++) {
			System.out.println(users.get(i));
		}
		model.addAttribute("users", users);
		return "admin/member-list"; 
	}
	
}
