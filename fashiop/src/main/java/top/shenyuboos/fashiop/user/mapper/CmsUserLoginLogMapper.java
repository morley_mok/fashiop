package top.shenyuboos.fashiop.user.mapper;

import top.shenyuboos.fashiop.user.entity.CmsUser;
import top.shenyuboos.fashiop.user.entity.CmsUserLoginLog;

import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 管理员登陆表 Mapper 接口
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface CmsUserLoginLogMapper extends BaseMapper<CmsUserLoginLog> {
	
	/**
	 * 后台登录
	 */
	@Select("SELECT * FROM cms_user WHERE `login_name`=#{loginName} AND `login_password`=#{loginPassword}")
	CmsUser CmsUserLogin(CmsUser cmsUser);
	
	
}
