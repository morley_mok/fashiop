package top.shenyuboos.fashiop.user.service;

import top.shenyuboos.fashiop.system.entity.CmsRole;
import top.shenyuboos.fashiop.user.entity.OsFavorite;
import top.shenyuboos.fashiop.user.entity.OsUser;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface IOsUserService extends IService<OsUser> {

	OsUser findUserByName(String username);
	
	CmsRole getRole(Long userId);
	/**
	 * 查询所有用户信息
	 * @return
	 */
	List<OsUser> getUserAll();
	
	/**
	 * 查询所有用户信息
	 * @return
	 */
	List<OsUser> getAllUser();
	
	
}
