package top.shenyuboos.fashiop.user.mapper;

import top.shenyuboos.fashiop.user.entity.OsFavorite;
import top.shenyuboos.fashiop.user.entity.OsUser;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface OsUserMapper extends BaseMapper<OsUser> {
	@Select("SELECT `user_id`,`user_name`,`sex`,`telephone`,email,`regeist_time` FROM `os_user` ")
	List<OsUser> getUserAll();
	
	List<OsUser> getAllUser();
	
}
