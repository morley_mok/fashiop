package top.shenyuboos.fashiop.user.controller;


import java.io.File;
import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.Query;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sun.management.OperatingSystemMXBean;

import top.shenyuboos.fashiop.user.entity.CmsUser;
import top.shenyuboos.fashiop.user.service.ICmsUserService;


/**
 * <p>
 * 管理员表 前端控制器
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@SuppressWarnings("restriction")
@Controller
@RequestMapping({"/user/cms-user","/"})
public class CmsUserController {

	private static final Logger log = Logger.getLogger(CmsUserController.class);
	
	@Autowired
	private ICmsUserService cmsUserService;
	
	@Autowired
    private RedisTemplate<String, String> redisTemplate;
	
	@RequestMapping("payindex")
	public String payindex() {
		return "payindex";
	}
	
	@RequestMapping("checkout")
	public String checkout() {
		return "checkout";
	}
	
	@RequestMapping("admin")
	public String admin(HttpServletRequest request) {
		Map<String, String> sysInfo = getServiceInfo();
		ServletContext application = request.getServletContext();	
		String upTime = getServiceStartTime((Long)application.getAttribute("startTime"));
		sysInfo.put("upTime", upTime);
		
		log.info(sysInfo);
		
		//增加访问量
		String click = redisTemplate.opsForValue().get("click");
		Integer thisClick = null;
		if (click == null || click.equals("")) {
			thisClick = 0;
		} else {
			thisClick = Integer.parseInt(click);
		}
		
		log.debug("当前点击量：" + thisClick);
		
		redisTemplate.opsForValue().set("click",(++thisClick).toString());
		
		request.setAttribute("sysInfo", sysInfo);
		return "admin/index";
	}
	
	@RequestMapping("login")
	public String login() {
		return "admin/login";
	}
	
	/**
	 * 后台登录
	 */
	@RequestMapping("cmslogin")
	@ResponseBody
	public int cmslogin(String username, String password, String checkCode, HttpServletRequest request) {
		log.info("入参：username=" + username + " password=" + password + " checkCode=" + checkCode);
		CmsUser cmsUser = new CmsUser();
		cmsUser.setLoginName(username);
		cmsUser.setLoginPassword(password);
		QueryWrapper<CmsUser> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("login_name", username);
		queryWrapper.eq("login_password", password);
		CmsUser cmslogin = cmsUserService.getOne(queryWrapper);
		String checkcode = request.getSession().getAttribute("checkCode").toString();
		if (!checkcode.equals(checkCode)) {
			return 1;// 1验证码错误
		} else if (cmslogin != null) {
			log.debug("登录成功！");
			request.getSession().setAttribute("user", cmslogin);
			return 2;// 2登录成功
		} else {
			return 3;// 3登录异常
		}
	}
	
	/**
	 * 获取服务器信息
	 */
	public Map<String, String> getServiceInfo() {
		Map<String, String> sysInfo = new HashMap<>();
		Properties sysprop = System.getProperties();
		
		try {
			InetAddress addr = InetAddress.getLocalHost();
	        sysInfo.put("computerName", addr.getHostName().toString()); //获取本机计算机名
	        sysInfo.put("ip", addr.getHostAddress().toString()); //获取本机ip
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		
		MBeanServer beanServer = ManagementFactory.getPlatformMBeanServer();
        Set<ObjectName> objectNames;
		try {
			objectNames = beanServer.queryNames(new ObjectName("*:type=Connector,*"),
			        Query.match(Query.attr("protocol"), Query.value("HTTP/1.1")));
			sysInfo.put("port", objectNames.iterator().next().getKeyProperty("port"));//获取本机端口
		} catch (MalformedObjectNameException e) {
			e.printStackTrace();
		}

		sysInfo.put("hostname", System.getenv().get("USERDOMAIN"));//获取域名
		
		sysInfo.put("projectPath", sysprop.getProperty("user.dir"));//获取项目路径
		sysInfo.put("osName", sysprop.getProperty("os.name"));//获取操作系统
		sysInfo.put("language", sysprop.getProperty("user.language"));//获取系统语言
		sysInfo.put("javaVersion", sysprop.getProperty("java.runtime.version"));//获取Java版本
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
		sysInfo.put("date", simpleDateFormat.format(new Date()));//获取系统当前日期
		
		File[] files = File.listRoots();
		StringBuffer filesRootPath = new StringBuffer();
		for (File file : files) {
			filesRootPath.append(file.getPath() + " ");
		}
		sysInfo.put("filesRootPath", filesRootPath.toString());//获取逻辑驱动器
		
		sysInfo.put("cpuNumber", String.valueOf(Runtime.getRuntime().availableProcessors()));//获取CPU数量
		
		OperatingSystemMXBean mem = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
		sysInfo.put("totalMemory", mem.getTotalPhysicalMemorySize() / 1024 / 1024 + "MB");//获取内存总量
		
		sysInfo.put("username", sysprop.getProperty("user.name"));//获取用户名
		
		return sysInfo;
	}

	/**
	 * 获取服务器启动时间
	 * @return
	 */
	public String getServiceStartTime(Long oldDate) {
		String upTime = "系统已正常运行%s小时%s分%s秒";
		Long time = System.currentTimeMillis() - oldDate;
		int h = (int) (time / (60 * 60 * 1000));
		int m = (int) (time / (60 * 1000)) - h * 60;
		int s = (int) (time / 1000 % 60);
		upTime = String.format(upTime, new Object[]{ h, m, s });
		
		return upTime;
	}
	
	
}
