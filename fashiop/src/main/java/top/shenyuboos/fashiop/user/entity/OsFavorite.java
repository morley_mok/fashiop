package top.shenyuboos.fashiop.user.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import top.shenyuboos.fashiop.product.entity.OsLabel;

/**
 * <p>
 * 收藏夹表
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-12-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class OsFavorite implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 收藏表ID
     */
    @TableId(value = "favorite_id", type = IdType.AUTO)
    private Long favoriteId;
    
	/**
     * 用户ID
     */
    private Long userId;

    /**
     * 商品ID
     */
    private Long productId;

    /**
     * 商品编号
     */
    private Long productNumber;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 展示图片
     */
    private String picImg;

    /**
     * 显示价格
     */
    private BigDecimal showPrice;

    /**
     * 商品状态：1,上架；2,下架
     */
    private Integer status;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 创建者
     */
    private String createBy;
   
    /**
     * 用户
     */
    @TableField(exist = false)
	private OsUser osUser;

}
