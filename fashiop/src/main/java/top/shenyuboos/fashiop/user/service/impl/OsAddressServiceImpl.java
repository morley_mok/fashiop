package top.shenyuboos.fashiop.user.service.impl;

import top.shenyuboos.fashiop.user.entity.OsAddress;
import top.shenyuboos.fashiop.user.mapper.OsAddressMapper;
import top.shenyuboos.fashiop.user.service.IOsAddressService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 收获地址表 服务实现类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-30
 */
@Service
public class OsAddressServiceImpl extends ServiceImpl<OsAddressMapper, OsAddress> implements IOsAddressService {

}
