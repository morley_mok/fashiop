package top.shenyuboos.fashiop.user.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 收获地址表 前端控制器
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-30
 */
@RestController
@RequestMapping("/user/os-address")
public class OsAddressController {

}
