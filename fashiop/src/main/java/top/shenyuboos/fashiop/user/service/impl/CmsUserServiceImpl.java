package top.shenyuboos.fashiop.user.service.impl;

import top.shenyuboos.fashiop.user.entity.CmsUser;
import top.shenyuboos.fashiop.user.mapper.CmsUserMapper;
import top.shenyuboos.fashiop.user.service.ICmsUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 管理员表 服务实现类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Service
public class CmsUserServiceImpl extends ServiceImpl<CmsUserMapper, CmsUser> implements ICmsUserService {
	
	
	@Override
	public CmsUser findUserByName(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	

}
