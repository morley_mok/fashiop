package top.shenyuboos.fashiop.user.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p>
 * 管理员登陆表 前端控制器
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Controller
@RequestMapping(value = { "/user/cms-user-login-log", "/" })
public class CmsUserLoginLogController {

}
