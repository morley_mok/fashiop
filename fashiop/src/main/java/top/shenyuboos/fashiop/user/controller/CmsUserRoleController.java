package top.shenyuboos.fashiop.user.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 管理员角色关联表 前端控制器
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@RestController
@RequestMapping("/user/cms-user-role")
public class CmsUserRoleController {

}
