package top.shenyuboos.fashiop.user.mapper;

import top.shenyuboos.fashiop.user.entity.OsAddress;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 收获地址表 Mapper 接口
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-30
 */
public interface OsAddressMapper extends BaseMapper<OsAddress> {

}
