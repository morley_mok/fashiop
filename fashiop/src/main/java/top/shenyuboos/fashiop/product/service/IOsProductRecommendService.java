package top.shenyuboos.fashiop.product.service;

import top.shenyuboos.fashiop.product.entity.OsProductRecommend;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品推荐表 服务类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface IOsProductRecommendService extends IService<OsProductRecommend> {

}
