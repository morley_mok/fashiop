package top.shenyuboos.fashiop.product.service;

import top.shenyuboos.fashiop.product.entity.OsSpecification;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 规格表
 服务类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-12-03
 */
public interface IOsSpecificationService extends IService<OsSpecification> {

}
