package top.shenyuboos.fashiop.product.service.impl;

import top.shenyuboos.fashiop.product.entity.OsProduct;
import top.shenyuboos.fashiop.product.mapper.OsProductMapper;
import top.shenyuboos.fashiop.product.service.IOsProductService;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import java.math.BigDecimal;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品表 服务实现类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Service
public class OsProductServiceImpl extends ServiceImpl<OsProductMapper, OsProduct> implements IOsProductService {
	
	@Autowired
	private OsProductMapper osProductMapper;
	
	@Override
	public List<OsProduct> selectProductPage(Page<OsProduct> page) {
	    return osProductMapper.selectPageVo(page);
	}

	@Override
	public List<OsProduct> findList(String productNumber,BigDecimal minPrice,BigDecimal maxPrice,String searchKey,Integer current) {
		Integer pageNum;
		if (current == null) {
			pageNum = 0;
		} else {
			pageNum = (current - 1) * 10;
		}
		return osProductMapper.findList(productNumber,minPrice,maxPrice,searchKey,pageNum);
	}

	@Override
	public Integer getCount(String productNumber, BigDecimal minPrice, BigDecimal maxPrice, String searchKey) {
		return osProductMapper.getCount(productNumber, minPrice, maxPrice, searchKey);
	}

	@Override
	public OsProduct getById(String productId) {
		return osProductMapper.getById(productId);
	}

	@Override
	public List<Map<String,Object>> getProDetailById(String productId) {
		return osProductMapper.getProDetailById(productId);
	}

	@Override
	public List<Map<String, Object>> selProByAll(int current,int pages,String sort,String cname) {
		int offset = (current-1)*pages;
		int limit = pages;
		return osProductMapper.selProByAll(offset, limit,sort,cname);
	}
	
}
