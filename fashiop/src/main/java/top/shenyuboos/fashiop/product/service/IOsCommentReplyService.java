package top.shenyuboos.fashiop.product.service;

import top.shenyuboos.fashiop.product.entity.OsCommentReply;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 评论回复表 服务类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-26
 */
public interface IOsCommentReplyService extends IService<OsCommentReply> {

}
