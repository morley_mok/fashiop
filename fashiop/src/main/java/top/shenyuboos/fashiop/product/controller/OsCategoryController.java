package top.shenyuboos.fashiop.product.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import top.shenyuboos.fashiop.product.entity.OsCategory;
import top.shenyuboos.fashiop.product.service.IOsCategoryService;
import top.shenyuboos.fashiop.product.service.IOsProductService;
import top.shenyuboos.fashiop.utils.CategoryTree;

/**
 * <p>
 * 分类表 前端控制器
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-12-04
 */
@Controller
@RequestMapping({"/product/os-category","/admin"})
public class OsCategoryController {
	
	private static final Logger log = Logger.getLogger(OsCategoryController.class);
	
	@Resource
	private IOsProductService osProductServiceImpl;
	
	@Resource
	private IOsCategoryService osCategoryService;
	
	@RequestMapping("oscategory")
	public String oscategory() {
		return "category";
	}
	
	/**
	 * 查询商品信息
	 */
	@ResponseBody
	@RequestMapping("showcategory")
	public Object showcategory(String current,String pages,String sort,String cname) {
		int curr;
		int page;
		curr=(current==null)?1:Integer.parseInt(current);
		page=(pages==null)?8:Integer.parseInt(pages);
		System.out.println(curr+"  "+page);
		System.out.println(osProductServiceImpl.selProByAll(curr, page,sort,cname));
		return osProductServiceImpl.selProByAll(curr, page,sort,cname);
	}
	
	/**
	 *	查询商品类型
	 */
	@ResponseBody
	@RequestMapping("selprocategroy")
	public Object selprocategroy() {
		System.err.println("controller"+osCategoryService.selProCategory());
		return osCategoryService.selProCategory();
	}
	
	/**
	 * @author 莫尚荣
	 * @return
	 */
	@RequestMapping("category")
	public String category(Model model) {
		model.addAttribute("count",osCategoryService.count());
		model.addAttribute("category", osCategoryService.list());
		return "admin/category";
	}
	
	@RequestMapping("getCategory")
	@ResponseBody
	public Object getCategory() {
		log.info("加载分类");
		List<OsCategory> categories = osCategoryService.list();
		
		CategoryTree categoryTree = new CategoryTree();
		
		//排序
		for (OsCategory osCategory : categories) {
			if (osCategory.getParentId().equals(0L)) {
				categoryTree.setCategoryId(osCategory.getCategoryId());
				categoryTree.setName(osCategory.getName());
			}
		}
		
		recursionCategory(categoryTree,categories);
		
		log.debug("分类排序后：" + categoryTree);
		
		return categoryTree;
	}
	
	/**
	 * 递归获取分类子分类
	 * @param categoryTree 当前元素
	 * @param categories 所有元素
	 */
	private static void recursionCategory(CategoryTree categoryTree, List<OsCategory> categories) {
		//将要添加的子元素
		CategoryTree children = null;
		
		//遍历所有元素
		for (OsCategory osCategory : categories) {
			//如果遍历元素的父ID与当前元素ID相同则为子分类
			if (osCategory.getParentId().equals(categoryTree.getCategoryId())) {
				//创建子元素属性
				children = new CategoryTree();
				children.setCategoryId(osCategory.getCategoryId());
				children.setName(osCategory.getName());
				
				if (categoryTree.getChildren() == null)
					categoryTree.setChildren(new ArrayList<>());
				
				//添加到当前元素
				categoryTree.getChildren().add(children);
				
				//重新递归，查询是否还有子元素
				recursionCategory(children,categories);
			} 
		}
	}
	
}
