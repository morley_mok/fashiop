package top.shenyuboos.fashiop.product.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商品图片表
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("os_product_image")
public class OsProductImage implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 商品图片ID
     */
    @TableId(value = "pic_img_id", type = IdType.AUTO)
    private Long picImgId;

    /**
     * 商品ID
     */
    private Long productId;

    /**
     * 展示图片
     */
    private String picImg;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 状态：1.显示；0.隐藏
     */
    private Integer status;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 创建者
     */
    private String createBy;


}
