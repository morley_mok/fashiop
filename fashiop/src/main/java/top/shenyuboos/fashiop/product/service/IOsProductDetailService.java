package top.shenyuboos.fashiop.product.service;

import top.shenyuboos.fashiop.product.entity.OsProductDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品描述表 服务类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface IOsProductDetailService extends IService<OsProductDetail> {
	
	
	/**
	 * 商品详情页single-product.html中的视频描述
	 */
	OsProductDetail productByIdDetail(String id);
	
}
