package top.shenyuboos.fashiop.product.service;

import top.shenyuboos.fashiop.product.entity.OsLabel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品标签表 服务类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-25
 */
public interface IOsLabelService extends IService<OsLabel> {

}
