package top.shenyuboos.fashiop.product.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商品推荐表
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("os_product_recommend")
public class OsProductRecommend implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 产品推荐ID
     */
    @TableId(value = "recommend_product_id", type = IdType.AUTO)
    private Long recommendProductId;

    /**
     * 推荐位ID
     */
    private Long recommendId;

    /**
     * 商品ID
     */
    private Long productId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 状态 1=显示/0=隐藏
     */
    private Integer status;

    /**
     * 推荐起始时间
     */
    private LocalDateTime beginTime;

    /**
     * 推荐结束时间
     */
    private LocalDateTime endTime;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 创建者
     */
    private String createBy;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 更新者
     */
    private String updateBy;


}
