package top.shenyuboos.fashiop.product.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 分类表
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-12-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class OsCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 分类ID
     */
    @TableId(value = "category_id", type = IdType.AUTO)
    private Long categoryId;

    /**
     * 父分类ID
     */
    private Long parentId;

    /**
     * 分类名称
     */
    private String name;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 目录类型 2=二级目录/1=一级目录/0=总目录
     */
    private Integer type;

    /**
     * 状态 1=显示/0=隐藏
     */
    private Integer status;

    /**
     * 是否导航栏 1=显示/0=隐藏
     */
    private Integer showInNav;

    /**
     * 是否置顶 1=置顶/0=默认
     */
    private Integer showInTop;

    /**
     * 是否热门 1=热门/0=默认
     */
    private Integer showInHot;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 创建者
     */
    private String createBy;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 更新者
     */
    private String updateBy;

    /**
     * 页面标题
     */
    private String pageTitle;

    /**
     * 页面描述
     */
    private String pageDescription;

    /**
     * 页面关键词
     */
    private String pageKeyword;

    /**
     * 备注信息
     */
    private String remarks;


}
