package top.shenyuboos.fashiop.product.service;

import top.shenyuboos.fashiop.product.entity.OsCategory;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 分类表 服务类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-12-04
 */
public interface IOsCategoryService extends IService<OsCategory> {

	/**
	 * 查询商品类型
	 * @return
	 */
	List<OsCategory> selProCategory();
	
}
