package top.shenyuboos.fashiop.product.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商品表
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class OsProduct implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 商品ID
	 */
	@TableId(value = "product_id", type = IdType.AUTO)
	private Long productId;

	/**
	 * 商品编号
	 */
	private Long productNumber;

	/**
	 * 标签ID
	 */
	private Integer labelId;
	
	/**
	 * 标签
	 */
	@TableField(exist = false)
	private OsLabel osLabel;

	/**
	 * 总数
	 */
	@TableField(exist = false)
	private Integer count;
	
	/**
	 * 好评数
	 */
	@TableField(exist = false)
	private Integer commentCounts;

	/**
	 * 商品名称
	 */
	private String name;

	/**
	 * 显示积分
	 */
	private Integer showScore;

	/**
	 * 显示价格
	 */
	private BigDecimal showPrice;

	/**
	 * 商品简介
	 */
	private String introduce;
	
	/**
	 * 商品描述
	 */
	@TableField(exist = false)
	private OsProductDetail osProductDetail;

	/**
	 * 展示图片
	 */
	private String picImg;
	
	/**
	 * 商品图片
	 */
	@TableField(exist = false)
	private List<OsProductImage> osProductImages;

	/**
	 * 是否置顶 1=置顶/0=默认
	 */
	private Integer showInTop;

	/**
	 * 是否导航栏 1=显示/0=隐藏
	 */
	private Integer showInNav;

	/**
	 * 是否热门 1=热门/0=默认
	 */
	private Integer showInHot;

	/**
	 * 是否上架：1=上架/0=下架
	 */
	private Integer showInShelve;

	/**
	 * 创建时间
	 */
	private LocalDateTime createTime;

	/**
	 * 创建者
	 */
	private String createBy;

	/**
	 * 上架时间
	 */
	private LocalDateTime shelveTime;

	/**
	 * 上架人
	 */
	private String shelveBy;

	/**
	 * 更新时间
	 */
	private LocalDateTime updateTime;

	/**
	 * 更新者
	 */
	private String updateBy;

	/**
	 * 搜索关键词
	 */
	private String searchKey;

	/**
	 * 页面标题
	 */
	private String pageTitle;

	/**
	 * 页面描述
	 */
	private String pageDescription;

	/**
	 * 页面关键词
	 */
	private String pageKeyword;

	/**
	 * 备注
	 */
	private String remarks;
	
	/**
	 * 商品属性
	 */
	private OsProductAttribute osProductAttribute;
	
	/**
	 * 商品规格
	 */
	private List<OsProductSpecification> osProductSpecifications;

}
