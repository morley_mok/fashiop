package top.shenyuboos.fashiop.product.service.impl;

import top.shenyuboos.fashiop.product.entity.OsCategory;
import top.shenyuboos.fashiop.product.mapper.OsCategoryMapper;
import top.shenyuboos.fashiop.product.service.IOsCategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 分类表 服务实现类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-12-04
 */
@Service
public class OsCategoryServiceImpl extends ServiceImpl<OsCategoryMapper, OsCategory> implements IOsCategoryService {

	@Resource
	private OsCategoryMapper osCategoryMapper;
	
	@Override
	public List<OsCategory> selProCategory() {
		System.err.println("service"+osCategoryMapper.selprocategroy());
		return osCategoryMapper.selprocategroy();
	}

	
}
