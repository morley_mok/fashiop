package top.shenyuboos.fashiop.product.mapper;

import top.shenyuboos.fashiop.product.entity.OsProductDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品描述表 Mapper 接口
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface OsProductDetailMapper extends BaseMapper<OsProductDetail> {

}
