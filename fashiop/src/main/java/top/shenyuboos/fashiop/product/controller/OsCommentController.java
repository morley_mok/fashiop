package top.shenyuboos.fashiop.product.controller;


import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;

import top.shenyuboos.fashiop.product.entity.OsComment;
import top.shenyuboos.fashiop.product.entity.OsProduct;
import top.shenyuboos.fashiop.product.service.impl.OsCommentServiceImpl;

/**
 * <p>
 * 评价表 前端控制器
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-26
 */
@Controller
@RequestMapping(value= {"/product/os-comment","/admin"})
public class OsCommentController {


	private static final Logger log = Logger.getLogger(OsCommentController.class);
	@Autowired
	private OsCommentServiceImpl commentServiceImpl;
	
	@RequestMapping("comment")
	public String comment(Model model) {
		model.addAttribute("count",commentServiceImpl.getCount());
		return "admin/comment";
	}
	
	/**
	 * @author 李杨
	 * @return
	 */
	@RequestMapping("/product-comment")
	@ResponseBody
	public Object getCommentList() {
		List<OsProduct> osProducts=commentServiceImpl.getProductComment();
		return osProducts;
	}
	
	
	/**
	 * @author 李杨
	 * @return
	 */
	@RequestMapping("/getCommentList")
	@ResponseBody
	public Object getProductList(HttpServletRequest request,String productNumber,BigDecimal minPrice,BigDecimal maxPrice,Integer current,String searchKey,Boolean searchProduct) {
		log.debug("入参：" + "productNumber " + productNumber + "  minPrice " + minPrice + "  maxPrice " + maxPrice + "  searchKey " + searchKey + "  current " + current);
		List<OsProduct> products = null;
		//request中searchProduct若为空证明是按传入条件查询第一页，若不为空则为查询request中条件与传入页数
		if (searchProduct != null) {
			request.setAttribute("searchProduct", null);
		}
		if (request.getAttribute("searchProduct") == null) {
			request.setAttribute("productId", productNumber);
			request.setAttribute("minPrice", minPrice);
			request.setAttribute("maxPrice", maxPrice);
			request.setAttribute("searchKey", searchKey);
			request.setAttribute("searchProduct", true);
		} else {
			productNumber = (String) request.getAttribute("productId");
			minPrice = (BigDecimal) request.getAttribute("minPrice");
			maxPrice = (BigDecimal) request.getAttribute("maxPrice");
			searchKey = (String) request.getAttribute("searchKey");
		}
		
		Integer count = commentServiceImpl.getCount();
		log.debug("记录数：" + count);
		
		request.getSession().setAttribute("count", count);
		
		products = commentServiceImpl.findList(productNumber,minPrice,maxPrice,searchKey,current);

		products.forEach(name -> log.debug(name));
		return JSON.toJSON(products);
	}
	
	@RequestMapping("/getCount")
	@ResponseBody
	public Object getCount(HttpSession session) {
		Integer count = (Integer) session.getAttribute("count");
		log.debug("获取的值：" + count);
		return count;
	}
	
	/**
	 * @author 李杨
	 */
	@RequestMapping("/comments")
	public Object getComment(HttpSession session,Model model,@RequestParam(value="productId") String productId) {
		List<OsComment> comment=commentServiceImpl.findComment(productId);
		session.setAttribute("productId", productId);
		for (OsComment osComment : comment) {
			if(osComment.getType()==1) {
				osComment.setTypes("优质"); 
			}else {
				osComment.setTypes("普通");
			}
			model.addAttribute("type",osComment.getTypes());
		}
		model.addAttribute("count", commentServiceImpl.findCount(productId));
		model.addAttribute("comment",comment);
		return "admin/comments";
	}
	
	/**
	 * @author 李杨
	 */
	@RequestMapping("/del")
	@ResponseBody
	public Object del(HttpSession session,Model model,@RequestParam(value="commentId") String commentId) {
		String productId=(String) session.getAttribute("productId");
		log.debug(productId);
		boolean result=commentServiceImpl.removeById(commentId);
		Integer count=commentServiceImpl.findCount(productId);
		session.setAttribute("count", count);
		log.debug(result);
		return result;
	}
	
	@RequestMapping("getCounts")
	@ResponseBody
	public Object getCounts(HttpSession session) {
		Integer count = (Integer) session.getAttribute("count");
		log.debug("获取的值：" + count);
		return count;
	}
	
	@RequestMapping("/dels")
	@ResponseBody
	public Object dels(HttpSession session,Model model,@RequestParam(value="commentId") List<String>commentId) {
		String productId=(String) session.getAttribute("productId");
		log.debug(productId);
		log.debug(commentId);
		boolean result=commentServiceImpl.removeByIds(commentId);
		model.addAttribute("count", commentServiceImpl.findCount(productId));
		log.debug(result);
		return result;
	}
	
}
