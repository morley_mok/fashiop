package top.shenyuboos.fashiop.product.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ResponseBody;
import top.shenyuboos.fashiop.product.service.IOsShoppingCartService;

/**
 * <p>
 * 购物车表 前端控制器
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-12-18
 */
@Controller
@RequestMapping("/product/os-shopping-cart")
public class OsShoppingCartController {

	@RequestMapping("cart")
	public String cart() {
		return "cart";
	}

	@Resource
	private IOsShoppingCartService OsShoppingCartServiceImpl;

	/**
	 * 商品加入购物车
	 */
	@ResponseBody
	@RequestMapping("addcart")
	public Integer addcart(String productId, HttpServletRequest request) {
		Long userId = request.getSession().getAttribute("userId") == null ? 1
				: (Long) request.getSession().getAttribute("userId");
		return OsShoppingCartServiceImpl.insert(productId, userId);// 1,2,3
	}

	@ResponseBody
	@RequestMapping("chese")
	public Object addcart(String productId) {
		return OsShoppingCartServiceImpl.selectByProIdSpec(productId);
	}
}
