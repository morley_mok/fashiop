package top.shenyuboos.fashiop.product.service.impl;

import top.shenyuboos.fashiop.product.entity.OsProductParameter;
import top.shenyuboos.fashiop.product.mapper.OsProductParameterMapper;
import top.shenyuboos.fashiop.product.service.IOsProductParameterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品参数表 服务实现类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Service
public class OsProductParameterServiceImpl extends ServiceImpl<OsProductParameterMapper, OsProductParameter> implements IOsProductParameterService {

}
