package top.shenyuboos.fashiop.product.service.impl;

import top.shenyuboos.fashiop.product.entity.OsProductImage;
import top.shenyuboos.fashiop.product.mapper.OsProductImageMapper;
import top.shenyuboos.fashiop.product.service.IOsProductImageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品图片表 服务实现类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Service
public class OsProductImageServiceImpl extends ServiceImpl<OsProductImageMapper, OsProductImage> implements IOsProductImageService {

	private OsProductImageMapper imageMapper;
	
	public OsProductImageMapper getImageMapper() {
		return imageMapper;
	}

	public void setImageMapper(OsProductImageMapper imageMapper) {
		this.imageMapper = imageMapper;
	}

	@Override
	public List<OsProductImage> getById(Integer id) {
		// TODO Auto-generated method stub
		return imageMapper.getById(id);
	}

}
