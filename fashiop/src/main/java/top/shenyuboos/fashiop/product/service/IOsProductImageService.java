package top.shenyuboos.fashiop.product.service;

import top.shenyuboos.fashiop.product.entity.OsProductImage;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品图片表 服务类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface IOsProductImageService extends IService<OsProductImage> {
	//根据商品Id查询图片
	public List<OsProductImage> getById(Integer id);
}
