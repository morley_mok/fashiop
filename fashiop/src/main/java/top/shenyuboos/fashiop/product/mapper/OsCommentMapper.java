package top.shenyuboos.fashiop.product.mapper;

import top.shenyuboos.fashiop.product.entity.OsComment;
import top.shenyuboos.fashiop.product.entity.OsProduct;

import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 评价表 Mapper 接口
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-26
 */
public interface OsCommentMapper extends BaseMapper<OsComment> {

	
    //商品信息及好评数
	@Select("SELECT `os_product`.`product_id`,`os_product`.`product_number`,`os_product`.`name`,`os_product`.`show_price`,`os_product`.`shelve_time`,`os_product`.`show_in_shelve`,SUM(`os_comment`.`good_count`) AS `count`\n" + 
			"FROM `os_comment`,`os_product`\n" + 
			"WHERE `os_comment`.`product_id`=`os_product`.`product_id`\n" + 
			"GROUP BY`os_comment`.`good_count`")
	public List<OsProduct> getProductComment();
	/**
	 * 搜索商品
	 * @return
	 */
	public List<OsProduct> findList(@Param("productNumber")String productNumber, @Param("minPrice")BigDecimal minPrice,
			@Param("maxPrice")BigDecimal maxPrice,@Param("searchKey")String searchKey,@Param("pageNum")Integer pageNum);
	
	Integer getCount();
	
	Integer selCount(@Param("productNumber")String productNumber, @Param("minPrice")BigDecimal minPrice,
			@Param("maxPrice")BigDecimal maxPrice,@Param("searchKey")String searchKey);
	
	Integer findCount(@Param("productId")String productId);
	/**
	 * 根据商品ID查询评论
	 */
	public List<OsComment> findComment(String productId);

}
