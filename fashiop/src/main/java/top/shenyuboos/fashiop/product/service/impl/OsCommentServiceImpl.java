package top.shenyuboos.fashiop.product.service.impl;

import top.shenyuboos.fashiop.product.entity.OsComment;
import top.shenyuboos.fashiop.product.entity.OsProduct;
import top.shenyuboos.fashiop.product.mapper.OsCommentMapper;
import top.shenyuboos.fashiop.product.service.IOsCommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 评价表 服务实现类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-26
 */
@Service
public class OsCommentServiceImpl extends ServiceImpl<OsCommentMapper, OsComment> implements IOsCommentService {

	@Autowired
	private OsCommentMapper commentMapper;
	
	@Override
	public List<OsProduct> getProductComment() {
		// TODO Auto-generated method stub
		return commentMapper.getProductComment();
	}

	@Override
	public List<OsProduct> findList(String productNumber, BigDecimal minPrice, BigDecimal maxPrice, String searchKey,Integer current) {
		Integer pageNum;
		if (current == null) {
			pageNum = 0;
		} else {
			pageNum = (current - 1) * 10;
		}
		return commentMapper.findList(productNumber, minPrice, maxPrice, searchKey, pageNum);
	}

	@Override
	public List<OsComment> findComment(String productId) {
		// TODO Auto-generated method stub
		return commentMapper.findComment(productId);
	}

	@Override
	public Integer getCount() {
		// TODO Auto-generated method stub
		return commentMapper.getCount();
	}

	@Override
	public Integer findCount(String productId) {
		// TODO Auto-generated method stub
		return commentMapper.findCount(productId);
	}

	@Override
	public Integer selCount(String productNumber, BigDecimal minPrice, BigDecimal maxPrice, String searchKey) {
		// TODO Auto-generated method stub
		return commentMapper.selCount(productNumber, minPrice, maxPrice, searchKey);
	}

}
