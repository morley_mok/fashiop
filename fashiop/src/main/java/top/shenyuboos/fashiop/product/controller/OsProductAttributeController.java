package top.shenyuboos.fashiop.product.controller;



import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import top.shenyuboos.fashiop.product.service.impl.OsProductAttributeServiceImpl;

/**
 * <p>
 * 商品属性表 前端控制器
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Controller
@RequestMapping("/product/os-product-attribute")
public class OsProductAttributeController {
	
	@Resource
	private OsProductAttributeServiceImpl OsProductAttributeService;
	
	
}
