package top.shenyuboos.fashiop.product.controller;


import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import top.shenyuboos.fashiop.product.entity.OsLabel;
import top.shenyuboos.fashiop.product.service.IOsLabelService;

/**
 * <p>
 * 商品标签表 前端控制器
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-25
 */
@RestController
@RequestMapping("/product/os-label")
public class OsLabelController {
 
	@Autowired
	private IOsLabelService osLabelService;
	
	/**
	 * 根据标签名获取标签Id
	 * @param name
	 * @return
	 */
	@RequestMapping("getLabelId")
	public Object getLabelId(String name) {
		if (name == null) {
			return null;
		}
		QueryWrapper<OsLabel> queryWrapper = new QueryWrapper<>();
		queryWrapper.select("label_id").eq("label_name", name);
		Map<String, Object> id = osLabelService.getMap(queryWrapper);
		System.err.println(id);
		if (id == null) {
			return null;
		}
		return id.get("label_id");
	}
}
