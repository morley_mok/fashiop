package top.shenyuboos.fashiop.product.mapper;

import top.shenyuboos.fashiop.product.entity.OsSpecification;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 规格表
 Mapper 接口
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-12-03
 */
public interface OsSpecificationMapper extends BaseMapper<OsSpecification> {

}
