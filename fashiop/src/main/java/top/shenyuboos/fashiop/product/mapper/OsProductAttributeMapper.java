package top.shenyuboos.fashiop.product.mapper;

import top.shenyuboos.fashiop.product.entity.OsProduct;
import top.shenyuboos.fashiop.product.entity.OsProductAttribute;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品属性表 Mapper 接口
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface OsProductAttributeMapper extends BaseMapper<OsProductAttribute> {

	
	public OsProduct getProduct();
}
