package top.shenyuboos.fashiop.product.mapper;

import top.shenyuboos.fashiop.product.entity.OsShoppingCart;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 购物车表 Mapper 接口
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-12-18
 */
public interface OsShoppingCartMapper extends BaseMapper<OsShoppingCart> {
	
	/**
	 * 购物车数量+1
	 */
	@Update("UPDATE `os_shopping_cart` SET `buy_number`=`buy_number`+1 WHERE `product_spec_number`=#{productSpecNumber}")
	int updateByspec(Long productSpecNumber);
	
}
