package top.shenyuboos.fashiop.product.service.impl;

import top.shenyuboos.fashiop.product.entity.OsSpecificationAttribute;
import top.shenyuboos.fashiop.product.mapper.OsSpecificationAttributeMapper;
import top.shenyuboos.fashiop.product.service.IOsSpecificationAttributeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 规格属性表 服务实现类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-12-03
 */
@Service
public class OsSpecificationAttributeServiceImpl extends ServiceImpl<OsSpecificationAttributeMapper, OsSpecificationAttribute> implements IOsSpecificationAttributeService {

}
