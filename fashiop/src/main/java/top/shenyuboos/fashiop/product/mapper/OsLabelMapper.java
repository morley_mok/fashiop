package top.shenyuboos.fashiop.product.mapper;

import top.shenyuboos.fashiop.product.entity.OsLabel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品标签表 Mapper 接口
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-25
 */
public interface OsLabelMapper extends BaseMapper<OsLabel> {

}
