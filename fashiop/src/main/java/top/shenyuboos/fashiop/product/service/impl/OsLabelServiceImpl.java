package top.shenyuboos.fashiop.product.service.impl;

import top.shenyuboos.fashiop.product.entity.OsLabel;
import top.shenyuboos.fashiop.product.mapper.OsLabelMapper;
import top.shenyuboos.fashiop.product.service.IOsLabelService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品标签表 服务实现类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-25
 */
@Service
public class OsLabelServiceImpl extends ServiceImpl<OsLabelMapper, OsLabel> implements IOsLabelService {

}
