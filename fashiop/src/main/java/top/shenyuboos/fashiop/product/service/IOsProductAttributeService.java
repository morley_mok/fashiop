package top.shenyuboos.fashiop.product.service;

import top.shenyuboos.fashiop.product.entity.OsProductAttribute;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品属性表 服务类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface IOsProductAttributeService extends IService<OsProductAttribute> {

}
