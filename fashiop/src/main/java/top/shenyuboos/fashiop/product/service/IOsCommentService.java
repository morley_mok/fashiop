package top.shenyuboos.fashiop.product.service;

import top.shenyuboos.fashiop.product.entity.OsComment;
import top.shenyuboos.fashiop.product.entity.OsProduct;

import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 评价表 服务类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-26
 */
public interface IOsCommentService extends IService<OsComment> {

	//商品信息及好评数
	public List<OsProduct> getProductComment();
	
	//搜索商品
	public List<OsProduct> findList(@Param("productNumber")String productNumber, @Param("minPrice")BigDecimal minPrice,
			@Param("maxPrice")BigDecimal maxPrice,@Param("searchKey")String searchKey,@Param("pageNum")Integer pageNum);

	/**
	 * 根据商品ID查询评论
	 */
	public List<OsComment> findComment(String productId);
	

	Integer findCount(@Param("productId")String productId);
	
	Integer getCount();
	

	Integer selCount(@Param("productNumber")String productNumber, @Param("minPrice")BigDecimal minPrice,
			@Param("maxPrice")BigDecimal maxPrice,@Param("searchKey")String searchKey);

}
