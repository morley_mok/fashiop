package top.shenyuboos.fashiop.product.service.impl;

import top.shenyuboos.fashiop.product.entity.OsProductCategory;
import top.shenyuboos.fashiop.product.mapper.OsProductCategoryMapper;
import top.shenyuboos.fashiop.product.service.IOsProductCategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品分类关联表 服务实现类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Service
public class OsProductCategoryServiceImpl extends ServiceImpl<OsProductCategoryMapper, OsProductCategory> implements IOsProductCategoryService {

}
