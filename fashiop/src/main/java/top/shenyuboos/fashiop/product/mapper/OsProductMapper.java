package top.shenyuboos.fashiop.product.mapper;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import top.shenyuboos.fashiop.product.entity.OsProduct;

/**
 * <p>
 * 商品表 Mapper 接口
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface OsProductMapper extends BaseMapper<OsProduct> {
	
	/**
     * <p>
     * 查询 : 根据stat，分页显示e状态查询商品列表
     * 注意!!: 如果入参是有多个,需要加注解指定参数名才能在xml中取值
     * </p>
     *
     * @param page 分页对象,xml中可以从里面进行取值,传递参数 Page 即自动分页,必须放在第一位(你可以继承Page实现自己的分页对象)
     * @param state 状态
     * @return 分页对象
     */
	@Select("SELECT * FROM `os_product` WHERE `show_in_hot`=1")
	List<OsProduct> selectPageVo(Page<OsProduct> page);
	
	/**
	 * @author 莫尚荣
	 * @return
	 */
	List<OsProduct> findList(@Param("productNumber")String productNumber, @Param("minPrice")BigDecimal minPrice,
			@Param("maxPrice")BigDecimal maxPrice,@Param("searchKey")String searchKey,@Param("pageNum")Integer pageNum);
	
	
	Integer getCount(@Param("productNumber")String productNumber, @Param("minPrice")BigDecimal minPrice,
			@Param("maxPrice")BigDecimal maxPrice,@Param("searchKey")String searchKey);

	
	/**
	 * 详情页根据ID查询
	 * @param productId
	 * @return
	 */
	OsProduct getById(String productId);
	
	/**
	 *	前台根据商品ID查询商品详细展示
	 */
	List<Map<String,Object>> getProDetailById(String productId);
	
	
	/**
	 * 前台分类页面  多条件查询
	 */
	List<Map<String,Object>> selProByAll(int offset,int limit,String sort,String cname);
}
