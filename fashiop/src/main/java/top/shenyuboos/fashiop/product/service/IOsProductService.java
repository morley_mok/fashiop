package top.shenyuboos.fashiop.product.service;

import top.shenyuboos.fashiop.product.entity.OsProduct;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品表 服务类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface IOsProductService extends IService<OsProduct> {
	/**
	 * 特色产品分页
	 */
	List<OsProduct> selectProductPage(Page<OsProduct> page);
	
	/**
	 * 模糊查询商品
	 * @author 莫尚荣
	 * @return
	 */
	List<OsProduct> findList(String productNumber,BigDecimal minPrice,BigDecimal maxPrice,String searchKey,Integer current);
	
	/**
	 * 获取查询记录数
	 * @param productId
	 * @param minPrice
	 * @param maxPrice
	 * @param searchKey
	 * @return
	 */
	Integer getCount(String productNumber,BigDecimal minPrice,BigDecimal maxPrice,String searchKey);
	
	/**
	 * 详情页根据ID查询
	 * @param productId
	 * @return
	 */
	OsProduct getById(String productId);
	
	/**
	 *	前台根据商品ID查询商品详细展示
	 */
	List<Map<String,Object>> getProDetailById(String productId);
	
	
	/**
	 * 前台分类页多条件查询
	 */
	List<Map<String, Object>> selProByAll(int current,int pages,String sort,String cname);

}
