package top.shenyuboos.fashiop.product.mapper;

import top.shenyuboos.fashiop.product.entity.OsCommentReply;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 评论回复表 Mapper 接口
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-26
 */
public interface OsCommentReplyMapper extends BaseMapper<OsCommentReply> {

}
