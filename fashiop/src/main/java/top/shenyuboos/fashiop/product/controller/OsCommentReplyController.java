package top.shenyuboos.fashiop.product.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 评论回复表 前端控制器
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-26
 */
@RestController
@RequestMapping("/product/os-comment-reply")
public class OsCommentReplyController {

}
