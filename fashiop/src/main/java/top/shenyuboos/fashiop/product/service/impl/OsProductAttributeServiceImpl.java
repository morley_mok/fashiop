package top.shenyuboos.fashiop.product.service.impl;

import top.shenyuboos.fashiop.product.entity.OsProductAttribute;
import top.shenyuboos.fashiop.product.mapper.OsProductAttributeMapper;
import top.shenyuboos.fashiop.product.service.IOsProductAttributeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品属性表 服务实现类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Service("OsProductAttributeService")
public class OsProductAttributeServiceImpl extends ServiceImpl<OsProductAttributeMapper, OsProductAttribute> implements IOsProductAttributeService {

	@Resource
	private OsProductAttributeMapper osProductAttributeMapper;
	

}
