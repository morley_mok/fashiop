package top.shenyuboos.fashiop.product.service;

import top.shenyuboos.fashiop.product.entity.OsProductCategory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品分类关联表 服务类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface IOsProductCategoryService extends IService<OsProductCategory> {

}
