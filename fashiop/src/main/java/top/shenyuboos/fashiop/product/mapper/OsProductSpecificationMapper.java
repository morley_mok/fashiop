package top.shenyuboos.fashiop.product.mapper;

import top.shenyuboos.fashiop.product.entity.OsProductSpecification;

import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品规格表 Mapper 接口
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface OsProductSpecificationMapper extends BaseMapper<OsProductSpecification> {
	
	@Select("SELECT * FROM os_product_specification WHERE product_id = #{productId} LIMIT 0,1")
	OsProductSpecification selectByProIdSpec(String productId);
	
}
