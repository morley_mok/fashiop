package top.shenyuboos.fashiop.product.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import top.shenyuboos.fashiop.product.entity.OsSpecificationAttribute;
import top.shenyuboos.fashiop.product.service.IOsSpecificationAttributeService;

/**
 * <p>
 * 规格属性表 前端控制器
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-12-03
 */
@RestController
@RequestMapping("/product/os-specification-attribute")
public class OsSpecificationAttributeController {

	@Autowired
	public IOsSpecificationAttributeService osSpecificationAttributeService;
	
	/**
	 * 根据规格编号查询列表
	 * @param specId
	 * @return
	 */
	@RequestMapping("getBySpecId")
	public Object getBySpecId(String specId) {
		QueryWrapper<OsSpecificationAttribute> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("specification_id", specId);
		return osSpecificationAttributeService.listObjs(queryWrapper);
	}
}
