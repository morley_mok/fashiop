package top.shenyuboos.fashiop.product.service;

import top.shenyuboos.fashiop.product.entity.OsProductSpecification;

import java.util.Map;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品规格表 服务类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface IOsProductSpecificationService extends IService<OsProductSpecification> {
	
	Map<String, OsProductSpecification> getByIds(String[] specIds);
	
	/**
	 * 查询商品规格详情
	 */
	Map<String, Object> selProSpec(String productId);
	
}
