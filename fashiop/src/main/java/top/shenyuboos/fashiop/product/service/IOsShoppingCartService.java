package top.shenyuboos.fashiop.product.service;

import top.shenyuboos.fashiop.product.entity.OsProductSpecification;
import top.shenyuboos.fashiop.product.entity.OsShoppingCart;
import top.shenyuboos.fashiop.product.mapper.OsShoppingCartMapper;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 购物车表 服务类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-12-18
 */
public interface IOsShoppingCartService extends IService<OsShoppingCart> {

	int SHOPING_SUCCESS=1;//插入成功
	int SHOPING_UPDATE=2;//数量加1
	int SHOPING_ERROE=3;//插入失败
	
	/**
	 * 添加商品到购物车
	 * @param osShoppingCart
	 * @return
	 */
	Integer insert(String productId,Long userId);
	
	OsProductSpecification selectByProIdSpec(String productId);
	
}
