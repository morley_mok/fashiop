package top.shenyuboos.fashiop.product.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商品属性表
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("os_product_attribute")
public class OsProductAttribute implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 属性ID
     */
    @TableId(value = "attribute_id", type = IdType.AUTO)
    private Long attributeId;

    /**
     * 商品ID
     */
    private Long productId;

    /**
     * 总库存
     */
    private Integer stock;

    /**
     * 销售量
     */
    private Integer salesVolume;

    /**
     * 游览量
     */
    private Integer pageViews;

    /**
     * 评论数量
     */
    private Integer commentNumber;

    /**
     * 累计评价
     */
    private Integer commentTotal;

    /**
     * 平均评价
     */
    private BigDecimal commentAverage;

    /**
     * 收藏数
     */
    private Integer favoriteNumber;

    /**
     * 提问数
     */
    private Integer questionNumber;


}
