package top.shenyuboos.fashiop.product.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import top.shenyuboos.fashiop.product.entity.OsProduct;
import top.shenyuboos.fashiop.product.entity.OsProductImage;
import top.shenyuboos.fashiop.product.service.IOsLabelService;
import top.shenyuboos.fashiop.product.service.IOsProductImageService;
import top.shenyuboos.fashiop.product.service.IOsProductService;
import top.shenyuboos.fashiop.product.service.IOsSpecificationService;

/**
 * <p>
 * 商品表 前端控制器
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Controller
@RequestMapping(value = { "/product/os-product", "/admin" })
public class OsProductController {

	private static final Logger log = Logger.getLogger(OsProductController.class);

	@Autowired
	private IOsProductService osProductService;

	@Autowired
	private IOsLabelService osLabelService;
	
	@Autowired
	private IOsProductImageService osProductImageService;

	@Autowired
	private IOsSpecificationService osSpecificationService;

	@RequestMapping("product")
	public String product(Model model) {
		model.addAttribute("count", osProductService.count());
		return "admin/product";
	}

	@ResponseBody
	@RequestMapping("showProductHot")
	public Object showProductInHot(int current) {
		Page<OsProduct> page = new Page<OsProduct>();
		page.setCurrent(current);// 当前页
		page.setSize(10);
		page.setRecords(osProductService.selectProductPage(page));
		return page;
	}

	/* =======================后台=========================== */
	/**
	 * @author 莫尚荣
	 * @return
	 */
	@RequestMapping("/getProductList")
	@ResponseBody
	public String getProductList(HttpServletRequest request, String productNumber, BigDecimal minPrice,
			BigDecimal maxPrice, String searchKey, Integer current, Boolean searchProduct) {
		log.debug("入参：" + "productNumber " + productNumber + "  minPrice " + minPrice + "  maxPrice " + maxPrice
				+ "  searchKey " + searchKey + "  current " + current);
		List<OsProduct> products = null;
		// request中searchProduct若为空证明是按传入条件查询第一页，若不为空则为查询request中条件与传入页数
		if (searchProduct != null) {
			request.setAttribute("searchProduct", null);
		}
		if (request.getAttribute("searchProduct") == null) {
			request.setAttribute("productId", productNumber);
			request.setAttribute("minPrice", minPrice);
			request.setAttribute("maxPrice", maxPrice);
			request.setAttribute("searchKey", searchKey);
			request.setAttribute("searchProduct", true);
		} else {
			productNumber = (String) request.getAttribute("productId");
			minPrice = (BigDecimal) request.getAttribute("minPrice");
			maxPrice = (BigDecimal) request.getAttribute("maxPrice");
			searchKey = (String) request.getAttribute("searchKey");
		}

		Integer count = osProductService.getCount(productNumber, minPrice, maxPrice, searchKey);
		log.debug("记录数：" + count);

		request.getSession().setAttribute("count", count);

		products = osProductService.findList(productNumber, minPrice, maxPrice, searchKey, current);

		products.forEach(name -> log.debug(name));
		return JSON.toJSONString(products);
	}

	@RequestMapping("getCount")
	@ResponseBody
	public Integer getCount(HttpSession session) {
		Integer count = (Integer) session.getAttribute("count");
		log.debug("获取的值：" + count);
		return count;
	}

	/**
	 * 商品详情
	 * 
	 * @param model
	 * @param productId
	 * @return
	 */
	@RequestMapping("product-show")
	public String product_show(Model model, String productId) {
		OsProduct osProduct = osProductService.getById(productId);
		log.debug("查询数据：" + osProduct);
		model.addAttribute(osProduct);
		return "admin/product-show";
	}

	@RequestMapping("outProduct")
	@ResponseBody
	public boolean outProduct(String productId) {
		OsProduct osProduct = new OsProduct().setProductId(Long.valueOf(productId)).setShowInShelve(0);
		return osProductService.updateById(osProduct);
	}

	@RequestMapping("upProduct")
	@ResponseBody
	public boolean upProduct(String productId) {
		OsProduct osProduct = new OsProduct().setProductId(Long.valueOf(productId)).setShowInShelve(1);
		return osProductService.updateById(osProduct);
	}

	/**
	 * 商品详情
	 */
	@RequestMapping("prodetailshow")
	public String getProDetailById(Model model, String productId) {
		model.addAttribute("prodetail", osProductService.getProDetailById(productId));
		return "single-product";
	}

	/**
	 * 更新商品
	 * 
	 * @param product
	 * @return
	 */
	@RequestMapping("saveProduct")
	@ResponseBody
	public boolean saveProduct(OsProduct product) {
		log.info("更新商品============================>");
		log.debug("入参：" + product);
		return osProductService.updateById(product);
	}

	/**
	 * 根据ID删除
	 * 
	 * @param ProductId
	 * @return
	 */
	@RequestMapping("delProduct")
	@ResponseBody
	public boolean delProduct(String productId) {
		log.debug("入参：" + productId);
		return osProductService.removeById(productId);
	}

	@RequestMapping("product-add")
	public String product_add(Model model) {
		model.addAttribute("labels", osLabelService.list());
		model.addAttribute("specs", osSpecificationService.list());
		return "admin/product-add";
	}

	@RequestMapping("addProduct")
	public String addProduct(Model model, MultipartFile coverImg, MultipartFile[] detaImg, String p_num, String p_name,
			String label, String spec_name, String spec_attr, String p_price, String p_introduce, String description,
			String remarks, HttpServletRequest request) {
		log.debug("商品添加模块(包含文件上传)：");

		log.debug("coverImg=" + coverImg);
		log.debug("detaImg=" + detaImg);
		log.debug("p_num=" + p_num);
		log.debug("p_name=" + p_name);
		log.debug("label=" + label);
		log.debug("spec_name=" + spec_name);
		log.debug("spec_attr=" + spec_attr);
		log.debug("p_price=" + p_price);
		log.debug("p_introduce=" + p_introduce);
		log.debug("description=" + description);
		log.debug("remarks=" + remarks);

		// log.debug("request="+request.getParameter("item_spec_name0"));

		if (coverImg.isEmpty()) {
			log.info("coverImg is empty");
			model.addAttribute("info", "coverImg is empty");
			return "/admin/addInfo";
		}

		Random random = new Random();
		
		String type = coverImg.getOriginalFilename().substring(coverImg.getOriginalFilename().indexOf("."));// 取文件格式后缀名
		String filename = System.currentTimeMillis() + (random.nextInt(9999)+10000) + type;// 取当前时间戳作为文件名
		log.debug("商品封面图片文件名：------> " + filename);
		String path = null;
		try {
			path = ResourceUtils.getURL("classpath:").getPath() + "static/img/product/" + filename;// 存放位置
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		log.debug("商品封面图片存放位置：------> " + path);
		try {
			OutputStream destFile = new FileOutputStream(new File(path));
			// FileCopyUtils.copy()这个方法里对IO进行了自动操作，不需要额外的再去关闭IO流
			FileCopyUtils.copy(coverImg.getInputStream(), destFile); // 复制临时文件到指定目录下
		} catch (IOException e) {
			e.printStackTrace();
		}


		// 构建商品对象
		OsProduct product = new OsProduct();
		product.setProductNumber(Long.valueOf(p_num));
		product.setLabelId(Integer.valueOf(label));
		product.setName(p_name);
		product.setIntroduce(p_introduce);
		product.setPageDescription(description);
		product.setPicImg(path);
		product.setShowPrice(new BigDecimal(p_price));
		product.setCreateTime(LocalDateTime.now());
		product.setCreateBy("admin");//备注，待修改
		product.setRemarks(remarks);
		
		//添加到数据库
		osProductService.save(product);
		
		// 商品图片
		if (detaImg != null) {
			OsProductImage productImage = null;
			for (int i = 0, dataImgLength = detaImg.length; i < dataImgLength; i++) {
				productImage = new OsProductImage();
				if (!detaImg[i].isEmpty()) {
					type = detaImg[i].getOriginalFilename().substring(detaImg[i].getOriginalFilename().indexOf("."));// 取文件格式后缀名
					filename = System.currentTimeMillis() + (random.nextInt(9999)+10000) + type;// 取当前时间戳作为文件名
					log.debug("商品图片"+i+"图片文件名：------> " + filename);
					try {
						path = ResourceUtils.getURL("classpath:").getPath() + "static/img/product/" + filename;// 存放位置
					} catch (FileNotFoundException e1) {
						e1.printStackTrace();
					}
					log.debug("商品图片"+i+"存放位置：------> " + path);
					productImage.setPicImg(path);
					try {
						OutputStream destFile = new FileOutputStream(new File(path));
						// FileCopyUtils.copy()这个方法里对IO进行了自动操作，不需要额外的再去关闭IO流
						FileCopyUtils.copy(coverImg.getInputStream(), destFile); // 复制临时文件到指定目录下
					} catch (IOException e) {
						e.printStackTrace();
					}
					
					productImage.setProductId(product.getProductId());
					productImage.setStatus(1);
					productImage.setCreateTime(LocalDateTime.now());
					productImage.setCreateBy("admin");//备注，待修改
					
					osProductImageService.save(productImage);
				}
			}
		} else {
			log.info("detaImg is empty");
			model.addAttribute("info", "detaImg is empty");
			return "/admin/addInfo";
		}
		
		model.addAttribute("info","添加成功！");
		return "/admin/addInfo";

	}

	/**
	 * 得到商品数量，便于前台判断页数
	 */
	@ResponseBody
	@RequestMapping("getprocount")
	public int procount() {
		return osProductService.count();
	}
	
}
