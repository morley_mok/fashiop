package top.shenyuboos.fashiop.product.service.impl;

import top.shenyuboos.fashiop.product.entity.OsSpecification;
import top.shenyuboos.fashiop.product.mapper.OsSpecificationMapper;
import top.shenyuboos.fashiop.product.service.IOsSpecificationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 规格表
 服务实现类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-12-03
 */
@Service
public class OsSpecificationServiceImpl extends ServiceImpl<OsSpecificationMapper, OsSpecification> implements IOsSpecificationService {

}
