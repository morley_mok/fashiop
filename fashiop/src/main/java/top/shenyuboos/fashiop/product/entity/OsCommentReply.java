package top.shenyuboos.fashiop.product.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 评论回复表
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class OsCommentReply implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 评论回复ID
     */
    @TableId(value = "comment_reply_id", type = IdType.AUTO)
    private Long commentReplyId;

    /**
     * 评论ID
     */
    private Long commentId;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 昵称
     */
    private String userName;

    /**
     * 用户头像
     */
    private String picImg;

    /**
     * 评论内容
     */
    private String content;

    /**
     * 好评数
     */
    private Integer goodCount;

    /**
     * 状态：1.显示；0.隐藏
     */
    private Integer status;

    /**
     * 评论类型：1,官方回复；0,用户回复
     */
    private Integer type;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 创建者
     */
    private String createBy;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 更新者
     */
    private String updateBy;


}
