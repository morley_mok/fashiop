package top.shenyuboos.fashiop.product.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 商品推荐表 前端控制器
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@RestController
@RequestMapping("/product/os-product-recommend")
public class OsProductRecommendController {

}
