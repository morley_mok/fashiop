package top.shenyuboos.fashiop.product.service.impl;

import top.shenyuboos.fashiop.product.entity.OsProductSpecification;
import top.shenyuboos.fashiop.product.entity.OsSpecification;
import top.shenyuboos.fashiop.product.entity.OsSpecificationAttribute;
import top.shenyuboos.fashiop.product.mapper.OsProductSpecificationMapper;
import top.shenyuboos.fashiop.product.mapper.OsSpecificationAttributeMapper;
import top.shenyuboos.fashiop.product.mapper.OsSpecificationMapper;
import top.shenyuboos.fashiop.product.service.IOsProductSpecificationService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品规格表 服务实现类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Service
public class OsProductSpecificationServiceImpl extends ServiceImpl<OsProductSpecificationMapper, OsProductSpecification> implements IOsProductSpecificationService {
	
	Logger logger = Logger.getLogger(OsProductSpecificationServiceImpl.class);
	
	@Autowired
	private OsProductSpecificationMapper osProductSpecificationMapper;
	
	@Autowired
	private OsSpecificationMapper osSpecificationMapper;
	
	@Autowired
	private OsSpecificationAttributeMapper osSpecificationAttributeMapper;
	
	
	@Override
	public Map<String, OsProductSpecification> getByIds(String[] specIds) {
		Map<String, OsProductSpecification> map = new HashMap<>();
		OsProductSpecification ps;
		OsSpecification s;
		OsSpecificationAttribute sa;
		for (String ids : specIds) {
			ps = new OsProductSpecification();
			ps.setOsSpecificationAttribute(new ArrayList<>());
			String[] arrIds = ids.split(",");
			for (String id : arrIds) {
				sa = new OsSpecificationAttribute().setSpecAttrId(Long.parseLong(id));
				sa = osSpecificationAttributeMapper.selectOne(new QueryWrapper<OsSpecificationAttribute>(sa));
				s = osSpecificationMapper.selectById(sa.getSpecificationId());
				sa.setOsSpecification(s);
				ps.getOsSpecificationAttribute().add(sa);
			}
			map.put(ids, ps);
		}
		return map;
	}

	/**
	 * 查询商品规格详情
	 */
	@Override
	public Map<String, Object> selProSpec(String productId) {
		logger.debug("===================>执行Service<=======================");
		Map<String, Object> map = new HashMap<>();
		//根据商品id查询商品的规格id
		List<OsProductSpecification> productSpecification = osProductSpecificationMapper.selectList(new QueryWrapper<OsProductSpecification>().eq("product_id", productId));
		
		for (OsProductSpecification os : productSpecification) {
			//根据os.getSpec()规格id查询规格
			List<OsSpecificationAttribute> sa = osSpecificationAttributeMapper.selectList(new QueryWrapper<OsSpecificationAttribute>().last("WHERE spec_attr_id in("+os.getSpec()+")"));
			String name = "";
			//例如：颜色和尺码整合
			for (OsSpecificationAttribute s : sa) {
				name += s.getName();
			}
			map.put(os.getSpec(), name);
			logger.debug("map:====>"+os.getSpec()+"===>"+name);
		}
		return map;
	}
}
