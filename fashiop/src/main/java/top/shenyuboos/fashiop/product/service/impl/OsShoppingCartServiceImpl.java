package top.shenyuboos.fashiop.product.service.impl;

import top.shenyuboos.fashiop.product.entity.OsProductSpecification;
import top.shenyuboos.fashiop.product.entity.OsShoppingCart;
import top.shenyuboos.fashiop.product.mapper.OsProductSpecificationMapper;
import top.shenyuboos.fashiop.product.mapper.OsShoppingCartMapper;
import top.shenyuboos.fashiop.product.service.IOsShoppingCartService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.time.LocalDateTime;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 购物车表 服务实现类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-12-18
 */
@Service
public class OsShoppingCartServiceImpl extends ServiceImpl<OsShoppingCartMapper, OsShoppingCart> implements IOsShoppingCartService {

	@Resource
	private OsShoppingCartMapper osShoppingCartMapper;
	
	@Resource
	private OsProductSpecificationMapper osProductSpecificationMapper;
	
	/**
	 * 购物车添加商品
	 */
	@Override
	public Integer insert(String productId,Long userId) {
		int result = 0;
		OsProductSpecification selectOne = osProductSpecificationMapper.selectByProIdSpec(productId);
		System.err.println(selectOne.getProductSpecNumber());
		List<OsShoppingCart> selectList = osShoppingCartMapper.selectList(null);
		int count = 0;
		for (OsShoppingCart osShoppingCart : selectList) {
			if(osShoppingCart.getProductSpecNumber().equals(selectOne.getProductSpecNumber())) {
				//判断该商品的该号规格是否已经添加入购物车
				count++;
			}
		}
		OsShoppingCart entity = new OsShoppingCart();
		entity.setProductSpecNumber(selectOne.getProductSpecNumber());
		entity.setUserId(userId);
		entity.setBuyNumber(1);
		entity.setCreateTime(LocalDateTime.now());
		if(count==0) {//插入
			result = (osShoppingCartMapper.insert(entity)>0)?SHOPING_SUCCESS:SHOPING_ERROE;//1,3;
		}else if(count>0){//修改
			result = (osShoppingCartMapper.updateByspec(selectOne.getProductSpecNumber())>0)?SHOPING_UPDATE:SHOPING_ERROE;//2,3
		}
		return result;
	}

	@Override
	public OsProductSpecification selectByProIdSpec(String productId) {
		return osProductSpecificationMapper.selectByProIdSpec(productId);
	}
}
