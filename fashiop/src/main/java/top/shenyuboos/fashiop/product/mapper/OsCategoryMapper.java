package top.shenyuboos.fashiop.product.mapper;

import top.shenyuboos.fashiop.product.entity.OsCategory;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 分类表 Mapper 接口
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-12-04
 */
public interface OsCategoryMapper extends BaseMapper<OsCategory> {
	
	
	/**
	 * 查询商品类型
	 */
	List<OsCategory> selprocategroy();
	
}
