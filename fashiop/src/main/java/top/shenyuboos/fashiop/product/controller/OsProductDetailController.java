package top.shenyuboos.fashiop.product.controller;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import top.shenyuboos.fashiop.product.service.IOsProductDetailService;

/**
 * <p>
 * 商品描述表 前端控制器
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@RestController
@RequestMapping("/product/os-product-detail")
public class OsProductDetailController {
	
	@Resource
	private IOsProductDetailService osProductDetailServiceImpl;
	
	/**
	 * 展示商品详情
	 */
	@RequestMapping("showproductDetail")
	@ResponseBody
	public Object showproductDetail(String productId) {
		return osProductDetailServiceImpl.productByIdDetail(productId);
	}
}
