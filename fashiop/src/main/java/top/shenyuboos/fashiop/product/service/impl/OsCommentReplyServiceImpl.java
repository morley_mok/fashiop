package top.shenyuboos.fashiop.product.service.impl;

import top.shenyuboos.fashiop.product.entity.OsCommentReply;
import top.shenyuboos.fashiop.product.mapper.OsCommentReplyMapper;
import top.shenyuboos.fashiop.product.service.IOsCommentReplyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 评论回复表 服务实现类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-26
 */
@Service
public class OsCommentReplyServiceImpl extends ServiceImpl<OsCommentReplyMapper, OsCommentReply> implements IOsCommentReplyService {

}
