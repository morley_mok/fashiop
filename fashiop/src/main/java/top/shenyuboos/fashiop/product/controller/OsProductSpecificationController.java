package top.shenyuboos.fashiop.product.controller;


import java.util.Arrays;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import top.shenyuboos.fashiop.product.entity.OsProductSpecification;
import top.shenyuboos.fashiop.product.service.IOsProductSpecificationService;

/**
 * <p>
 * 商品规格表 前端控制器
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@RestController
@RequestMapping("/product/os-product-specification")
public class OsProductSpecificationController {

	@Autowired
	private IOsProductSpecificationService osProductSpecificationService;
	
	private static final Logger log = Logger.getLogger(OsProductSpecificationController.class);
	
	@RequestMapping("getSpecs")
	public Object getSpecs(@RequestParam("specId[]") String[] specId) {
		log.debug("入参: specId=" + Arrays.toString(specId));
		Map<String, OsProductSpecification> map = osProductSpecificationService.getByIds(specId);
		log.debug("出参：" + map);
		return map;
	}
	
	@ResponseBody
	@RequestMapping("showProduSpec")
	public Object showProduSpec(String productId) {
		Map<String, Object> selProSpec = osProductSpecificationService.selProSpec(productId);
		return selProSpec;
	}
}
