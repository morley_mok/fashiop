package top.shenyuboos.fashiop.product.service.impl;

import top.shenyuboos.fashiop.product.entity.OsProductDetail;
import top.shenyuboos.fashiop.product.mapper.OsProductDetailMapper;
import top.shenyuboos.fashiop.product.service.IOsProductDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品描述表 服务实现类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Service
public class OsProductDetailServiceImpl extends ServiceImpl<OsProductDetailMapper, OsProductDetail> implements IOsProductDetailService {

	@Resource
	private OsProductDetailMapper osProductDetailMapper;
	
	/**
	 * 商品详情页single-product.html中的视频描述
	 */
	@Override
	public OsProductDetail productByIdDetail(String id) {
		return osProductDetailMapper.selectById(id);
	}

}
