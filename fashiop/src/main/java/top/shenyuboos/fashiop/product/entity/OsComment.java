package top.shenyuboos.fashiop.product.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 评价表
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class OsComment implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 评价ID
     */
    @TableId(value = "comment_id", type = IdType.AUTO)
    private Long commentId;

    /**
     * 商品ID
     */
    private Long productId;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 昵称
     */
    private String userName;

    /**
     * 用户头像
     */
    private String picImg;

    /**
     * 订单ID
     */
    private Long orderId;

    /**
     * 评论星级：1,2,3,4,5
     */
    private Integer star;

    /**
     * 评论内容
     */
    private String content;

    /**
     * 好评数
     */
    private Integer goodCount;

    /**
     * 状态：1.显示；0.隐藏
     */
    private Integer status;

    /**
     * 评论类型：1,优质；0,普通
     */
    private Integer type;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 创建者
     */
    private String createBy;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 更新者
     */
    private String updateBy;
    
    /**
     * 评论类型
     */
    private String types;
    
    


}
