package top.shenyuboos.fashiop.product.service.impl;

import top.shenyuboos.fashiop.product.entity.OsProductRecommend;
import top.shenyuboos.fashiop.product.mapper.OsProductRecommendMapper;
import top.shenyuboos.fashiop.product.service.IOsProductRecommendService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品推荐表 服务实现类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Service
public class OsProductRecommendServiceImpl extends ServiceImpl<OsProductRecommendMapper, OsProductRecommend> implements IOsProductRecommendService {

}
