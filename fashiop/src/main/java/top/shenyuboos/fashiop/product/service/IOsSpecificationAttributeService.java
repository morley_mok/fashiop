package top.shenyuboos.fashiop.product.service;

import top.shenyuboos.fashiop.product.entity.OsSpecificationAttribute;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 规格属性表 服务类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-12-03
 */
public interface IOsSpecificationAttributeService extends IService<OsSpecificationAttribute> {

}
