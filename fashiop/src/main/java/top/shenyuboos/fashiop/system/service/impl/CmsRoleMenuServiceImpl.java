package top.shenyuboos.fashiop.system.service.impl;

import top.shenyuboos.fashiop.system.entity.CmsRoleMenu;
import top.shenyuboos.fashiop.system.mapper.CmsRoleMenuMapper;
import top.shenyuboos.fashiop.system.service.ICmsRoleMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色权限关联表 服务实现类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Service
public class CmsRoleMenuServiceImpl extends ServiceImpl<CmsRoleMenuMapper, CmsRoleMenu> implements ICmsRoleMenuService {

}
