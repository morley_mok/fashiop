package top.shenyuboos.fashiop.system.service;

import top.shenyuboos.fashiop.system.entity.CmsRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色权限关联表 服务类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface ICmsRoleMenuService extends IService<CmsRoleMenu> {

}
