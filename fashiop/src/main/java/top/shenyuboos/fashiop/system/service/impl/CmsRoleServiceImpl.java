package top.shenyuboos.fashiop.system.service.impl;

import top.shenyuboos.fashiop.system.entity.CmsRole;
import top.shenyuboos.fashiop.system.mapper.CmsRoleMapper;
import top.shenyuboos.fashiop.system.service.ICmsRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Service
public class CmsRoleServiceImpl extends ServiceImpl<CmsRoleMapper, CmsRole> implements ICmsRoleService {

	@Override
	public List<CmsRole> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

}
