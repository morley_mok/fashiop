package top.shenyuboos.fashiop.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import top.shenyuboos.fashiop.system.entity.CmsUserRole;

/**
 * <p>
 * 管理员角色关联表 Mapper 接口
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface CmsUserRoleMapper extends BaseMapper<CmsUserRole> {

}
