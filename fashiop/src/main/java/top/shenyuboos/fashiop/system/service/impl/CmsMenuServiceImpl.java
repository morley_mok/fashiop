package top.shenyuboos.fashiop.system.service.impl;

import top.shenyuboos.fashiop.system.entity.CmsMenu;
import top.shenyuboos.fashiop.system.mapper.CmsMenuMapper;
import top.shenyuboos.fashiop.system.service.ICmsMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 目录表 服务实现类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Service
public class CmsMenuServiceImpl extends ServiceImpl<CmsMenuMapper, CmsMenu> implements ICmsMenuService {

}
