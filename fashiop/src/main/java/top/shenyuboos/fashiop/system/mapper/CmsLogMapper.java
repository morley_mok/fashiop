package top.shenyuboos.fashiop.system.mapper;

import top.shenyuboos.fashiop.system.entity.CmsLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 日志记录表 Mapper 接口
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface CmsLogMapper extends BaseMapper<CmsLog> {

}
