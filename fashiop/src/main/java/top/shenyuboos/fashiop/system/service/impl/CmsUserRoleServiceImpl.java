package top.shenyuboos.fashiop.system.service.impl;

import top.shenyuboos.fashiop.system.entity.CmsUserRole;
import top.shenyuboos.fashiop.system.mapper.CmsUserRoleMapper;
import top.shenyuboos.fashiop.system.service.ICmsUserRoleService;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 管理员角色关联表 服务实现类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Service
public class CmsUserRoleServiceImpl extends ServiceImpl<CmsUserRoleMapper, CmsUserRole> implements ICmsUserRoleService {

}
