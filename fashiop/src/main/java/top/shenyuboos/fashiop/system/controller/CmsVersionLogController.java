package top.shenyuboos.fashiop.system.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p>
 * 系统日志表 前端控制器
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Controller
@RequestMapping("/system/cms-version-log")
public class CmsVersionLogController {

	@Autowired
    private RedisTemplate<String, String> redisTemplate;
	
	@RequestMapping("echarts1")
	public String echarts1(Model model) {
		
		//获取访问量
		String click = redisTemplate.opsForValue().get("click");
		model.addAttribute("click",click);
		
		return "admin/echarts1";
	}
}
