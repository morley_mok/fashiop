package top.shenyuboos.fashiop.system.entity;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统配置表
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class CmsConfig implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * 系统配置ID
     */
    private Long configId;

    /**
     * 系统配置键
     */
    private String configKey;

    /**
     * 系统配置值
     */
    private String configValue;

    /**
     * 系统设置名称
     */
    private String configName;

    /**
     * 系统配置备注
     */
    private String remark;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 创建者
     */
    private String createBy;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 更新者
     */
    private String updateBy;


}
