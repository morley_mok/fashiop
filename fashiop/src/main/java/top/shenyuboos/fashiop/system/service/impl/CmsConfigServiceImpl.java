package top.shenyuboos.fashiop.system.service.impl;

import top.shenyuboos.fashiop.system.entity.CmsConfig;
import top.shenyuboos.fashiop.system.mapper.CmsConfigMapper;
import top.shenyuboos.fashiop.system.service.ICmsConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统配置表 服务实现类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Service
public class CmsConfigServiceImpl extends ServiceImpl<CmsConfigMapper, CmsConfig> implements ICmsConfigService {

}
