package top.shenyuboos.fashiop.system.mapper;

import top.shenyuboos.fashiop.system.entity.CmsConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统配置表 Mapper 接口
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface CmsConfigMapper extends BaseMapper<CmsConfig> {

}
