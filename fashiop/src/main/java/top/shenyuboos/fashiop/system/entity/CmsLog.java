package top.shenyuboos.fashiop.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 日志记录表
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class CmsLog implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 日志ID
     */
    @TableId(value = "log_id", type = IdType.AUTO)
    private Long logId;

    /**
     * 管理员ID
     */
    private Long userId;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 耗时
     */
    private Integer spendTime;

    /**
     * 请求类型
     */
    private String method;

    /**
     * 用户标识
     */
    private String userAgent;

    /**
     * 用户IP
     */
    private String userIp;

    /**
     * 请求内容
     */
    private String optContent;

    /**
     * 请求路径
     */
    private String url;


}
