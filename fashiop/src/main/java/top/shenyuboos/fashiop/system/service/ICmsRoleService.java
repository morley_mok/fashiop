package top.shenyuboos.fashiop.system.service;

import top.shenyuboos.fashiop.system.entity.CmsRole;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface ICmsRoleService extends IService<CmsRole> {

	List<CmsRole> findAll();
}
