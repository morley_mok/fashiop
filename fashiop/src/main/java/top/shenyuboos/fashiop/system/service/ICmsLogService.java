package top.shenyuboos.fashiop.system.service;

import top.shenyuboos.fashiop.system.entity.CmsLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 日志记录表 服务类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface ICmsLogService extends IService<CmsLog> {

}
