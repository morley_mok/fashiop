package top.shenyuboos.fashiop.system.mapper;

import top.shenyuboos.fashiop.system.entity.CmsRoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色权限关联表 Mapper 接口
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface CmsRoleMenuMapper extends BaseMapper<CmsRoleMenu> {

}
