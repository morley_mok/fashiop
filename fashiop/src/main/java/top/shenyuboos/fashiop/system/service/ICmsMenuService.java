package top.shenyuboos.fashiop.system.service;

import top.shenyuboos.fashiop.system.entity.CmsMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 目录表 服务类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface ICmsMenuService extends IService<CmsMenu> {

}
