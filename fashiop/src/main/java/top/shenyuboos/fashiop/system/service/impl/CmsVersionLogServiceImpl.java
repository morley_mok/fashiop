package top.shenyuboos.fashiop.system.service.impl;

import top.shenyuboos.fashiop.system.entity.CmsVersionLog;
import top.shenyuboos.fashiop.system.mapper.CmsVersionLogMapper;
import top.shenyuboos.fashiop.system.service.ICmsVersionLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统日志表 服务实现类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Service
public class CmsVersionLogServiceImpl extends ServiceImpl<CmsVersionLogMapper, CmsVersionLog> implements ICmsVersionLogService {

}
