package top.shenyuboos.fashiop.system.service.impl;

import top.shenyuboos.fashiop.system.entity.CmsLog;
import top.shenyuboos.fashiop.system.mapper.CmsLogMapper;
import top.shenyuboos.fashiop.system.service.ICmsLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 日志记录表 服务实现类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Service
public class CmsLogServiceImpl extends ServiceImpl<CmsLogMapper, CmsLog> implements ICmsLogService {

}
