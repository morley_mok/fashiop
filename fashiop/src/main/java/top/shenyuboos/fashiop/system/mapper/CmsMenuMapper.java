package top.shenyuboos.fashiop.system.mapper;

import top.shenyuboos.fashiop.system.entity.CmsMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 目录表 Mapper 接口
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface CmsMenuMapper extends BaseMapper<CmsMenu> {

}
