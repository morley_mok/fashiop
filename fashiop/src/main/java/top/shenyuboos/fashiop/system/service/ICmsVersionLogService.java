package top.shenyuboos.fashiop.system.service;

import top.shenyuboos.fashiop.system.entity.CmsVersionLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统日志表 服务类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface ICmsVersionLogService extends IService<CmsVersionLog> {

}
