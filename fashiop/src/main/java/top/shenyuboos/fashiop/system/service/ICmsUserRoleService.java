package top.shenyuboos.fashiop.system.service;

import com.baomidou.mybatisplus.extension.service.IService;

import top.shenyuboos.fashiop.system.entity.CmsUserRole;

/**
 * <p>
 * 管理员角色关联表 服务类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface ICmsUserRoleService extends IService<CmsUserRole> {

}
