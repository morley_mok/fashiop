package top.shenyuboos.fashiop.order.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 订单明细表 前端控制器
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@RestController
@RequestMapping("/order/os-order-product")
public class OsOrderProductController {

}
