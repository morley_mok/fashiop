package top.shenyuboos.fashiop.order.service;

import top.shenyuboos.fashiop.order.entity.OsOrderStatus;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单状态表 服务类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface IOsOrderStatusService extends IService<OsOrderStatus> {

}
