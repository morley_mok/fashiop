package top.shenyuboos.fashiop.order.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import top.shenyuboos.fashiop.order.entity.OsOrder;
import top.shenyuboos.fashiop.order.service.impl.OsOrderServiceImpl;


/**
 * <p>
 * 订单表 前端控制器
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Controller
@RequestMapping(value={"/order/os-order","/admin"})
public class OsOrderController {
	

	private static final Logger log = Logger.getLogger(OsOrderController.class);

	@RequestMapping("tracking")
	public String tracking(){
		System.out.println(1);
		return "tracking";
	}
	
	@Autowired
	private OsOrderServiceImpl osOrderServiceImpl;
	
	@RequestMapping("orderscount")
	public String orderscount(Model model) {
		System.err.println(1);
		model.addAttribute("count",osOrderServiceImpl.count());
		return "admin/order";
	}
	
	@RequestMapping("orderbynum")
	@ResponseBody
	public List<OsOrder> ordershow(String orderNumber,String username_telephone_email){
		System.err.println(orderNumber+":"+username_telephone_email);
		Long long1 = Long.parseLong(orderNumber);
		return osOrderServiceImpl.findAll(long1, username_telephone_email);
	}
	
	
	
	/**
	 * 查询所有并分页
	 *@author 柳海峰
	 * @return
	 */
	@RequestMapping("orderadmin")
	@ResponseBody
	public List<OsOrder> orders(HttpServletRequest request,String orderNumber,String username,Integer orderstatus,Integer current,Boolean searchOrder){
		log.debug(orderNumber+":"+username+":"+orderstatus+":"+current);
	if(("%%").equals(orderNumber)) {
		orderNumber=null;
	} else {
		orderNumber = "%" + orderNumber + "%";
	}
		List<OsOrder> orders = null;
		//request中searchProduct若为空证明是按传入条件查询第一页，若不为空则为查询request中条件与传入页数
		if (searchOrder != null) {
			request.setAttribute("searchOrder", null);
		}
		if (request.getAttribute("searchOrder") == null) {
			request.setAttribute("orderNumber", orderNumber);
			request.setAttribute("username", username);
			request.setAttribute("orderstatus", orderstatus);
			request.setAttribute("searchOrder", true);
		} else {
			orderNumber = (String) request.getAttribute("orderNumber");
			username = (String) request.getAttribute("username");
			orderstatus = (Integer) request.getAttribute("orderstatus");
		}
		
		Integer count = osOrderServiceImpl.getCount(orderNumber, username, orderstatus);
		log.debug("记录数：" + count);
		
		request.getSession().setAttribute("count", count);
		orders = osOrderServiceImpl.findAllOrder(orderNumber, username, orderstatus,current);
		
		log.debug(orderstatus);
		orders.forEach(name -> log.debug(name));
		return orders;
	}
	
	@RequestMapping("getCount")
	@ResponseBody
	public Object getCount(HttpSession session) {
		Integer count = (Integer) session.getAttribute("count");
		log.debug("获取的值：" + count);
		return count;
	}
	@RequestMapping("orders")
	@ResponseBody
	public Object orderObj() {
		List<OsOrder> list=osOrderServiceImpl.getAll();
		return list;
	}
	
		
		
	
	
	
}