package top.shenyuboos.fashiop.order.service.impl;

import top.shenyuboos.fashiop.order.entity.OsOrderStatus;
import top.shenyuboos.fashiop.order.mapper.OsOrderStatusMapper;
import top.shenyuboos.fashiop.order.service.IOsOrderStatusService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单状态表 服务实现类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Service
public class OsOrderStatusServiceImpl extends ServiceImpl<OsOrderStatusMapper, OsOrderStatus> implements IOsOrderStatusService {

}
