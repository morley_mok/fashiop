package top.shenyuboos.fashiop.order.service.impl;

import top.shenyuboos.fashiop.order.entity.OsOrderProduct;
import top.shenyuboos.fashiop.order.mapper.OsOrderProductMapper;
import top.shenyuboos.fashiop.order.service.IOsOrderProductService;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * <p>
 * 订单明细表 服务实现类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Service
public class OsOrderProductServiceImpl extends ServiceImpl<OsOrderProductMapper, OsOrderProduct> implements IOsOrderProductService {

}
