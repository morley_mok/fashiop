package top.shenyuboos.fashiop.order.mapper;

import top.shenyuboos.fashiop.order.entity.OsOrderProduct;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单明细表 Mapper 接口
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface OsOrderProductMapper extends BaseMapper<OsOrderProduct> {

}
