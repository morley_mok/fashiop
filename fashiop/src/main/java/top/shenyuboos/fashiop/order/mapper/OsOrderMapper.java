package top.shenyuboos.fashiop.order.mapper;

import top.shenyuboos.fashiop.order.entity.OsOrder;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface OsOrderMapper extends BaseMapper<OsOrder> {
	/**
	 * 根据条件查询订单表（前台订单追踪）
	 * @param orderId
	 * @param invoiceTitle
	 * @return
	 */
	 @Select("SELECT o.* FROM os_order o  INNER JOIN os_user u ON u.`user_id` = o.`user_id` WHERE o.order_number =#{order_number} AND (u.`user_name` = #{username_telephone_email}  OR u.`telephone` = #{username_telephone_email} OR u.`email` = #{username_telephone_email})")
	 List<OsOrder> findAll(@Param("order_number")Long orderNumber,
			 @Param("username_telephone_email")String username_telephone_email);
     /**
      *  根据条件查询订单表（后台订单列表）
      * @param orderNumber
      * @param useranme
      * @param orderstatus
      * @return
      */
	List<OsOrder> findAllOrder(@Param("orderNumber")String orderNumber,
			@Param("username")String username,@Param("orderstatus")Integer orderstatus,@Param("pageNum")Integer pageNum);
	/**
	 * 显示总数据信息分页
	 * @return
	 */
	List<OsOrder> getAll();
	
	/**
	 * 查询总条数
	 */
	Integer getCount(@Param("orderNumber")String orderNumber,@Param("username")String useranme,@Param("orderstatus")Integer orderstatus);
}
