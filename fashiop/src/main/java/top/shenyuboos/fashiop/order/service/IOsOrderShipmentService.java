package top.shenyuboos.fashiop.order.service;

import top.shenyuboos.fashiop.order.entity.OsOrderShipment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单配送表 服务类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface IOsOrderShipmentService extends IService<OsOrderShipment> {

}
