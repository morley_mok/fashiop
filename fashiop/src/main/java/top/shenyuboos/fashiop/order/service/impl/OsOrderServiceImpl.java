package top.shenyuboos.fashiop.order.service.impl;

import top.shenyuboos.fashiop.order.entity.OsOrder;
import top.shenyuboos.fashiop.order.mapper.OsOrderMapper;
import top.shenyuboos.fashiop.order.service.IOsOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单表 服务实现类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Service
public class OsOrderServiceImpl extends ServiceImpl<OsOrderMapper, OsOrder> implements IOsOrderService {

	@Autowired
	private OsOrderMapper osorderMapper;

	@Override
	public List<OsOrder> findAll(Long orderNumber, String username_telephone_email) {
		// TODO Auto-generated method stub
		return osorderMapper.findAll(orderNumber, username_telephone_email);
	}

	@Override
	public List<OsOrder> findAllOrder(String orderNumber, String username, Integer orderstatus,Integer current) {
		Integer pageNum;
		if (current == null) {
			pageNum = 0;
		} else {
			pageNum = (current - 1) * 10;
		}
		return osorderMapper.findAllOrder(orderNumber, username, orderstatus,pageNum);
	}

	@Override
	public List<OsOrder> getAll() {
		// TODO Auto-generated method stub
		return osorderMapper.getAll();
	}


	@Override
	public Integer getCount(String orderNumber, String useranme, Integer orderstatus) {
		// TODO Auto-generated method stub
		return osorderMapper.getCount(orderNumber, useranme, orderstatus);
	}
	

}
