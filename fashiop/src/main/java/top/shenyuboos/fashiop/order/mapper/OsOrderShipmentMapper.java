package top.shenyuboos.fashiop.order.mapper;

import top.shenyuboos.fashiop.order.entity.OsOrderShipment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单配送表 Mapper 接口
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface OsOrderShipmentMapper extends BaseMapper<OsOrderShipment> {

}
