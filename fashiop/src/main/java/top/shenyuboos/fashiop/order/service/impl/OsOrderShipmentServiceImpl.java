package top.shenyuboos.fashiop.order.service.impl;

import top.shenyuboos.fashiop.order.entity.OsOrderShipment;
import top.shenyuboos.fashiop.order.mapper.OsOrderShipmentMapper;
import top.shenyuboos.fashiop.order.service.IOsOrderShipmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单配送表 服务实现类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
@Service
public class OsOrderShipmentServiceImpl extends ServiceImpl<OsOrderShipmentMapper, OsOrderShipment> implements IOsOrderShipmentService {

}
