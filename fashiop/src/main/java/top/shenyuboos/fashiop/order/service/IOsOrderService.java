package top.shenyuboos.fashiop.order.service;

import top.shenyuboos.fashiop.order.entity.OsOrder;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface IOsOrderService extends IService<OsOrder> {
	//前台订单追踪
	public List<OsOrder> findAll(Long orderNumber,String username_telephone_email);
	
	//后台订单列表查询
	List<OsOrder> findAllOrder(@Param("order_number")String orderNumber,
			@Param("user_name")String useranme,@Param("order_status")Integer orderstatus,@Param("pageNum")Integer pageNum);
	
	List<OsOrder> getAll();
	

	/**
	 * 查询总条数
	 */
	Integer getCount(@Param("order_number")String orderNumber,@Param("user_name")String useranme,@Param("order_status")Integer orderstatus);
	
	
	
}
