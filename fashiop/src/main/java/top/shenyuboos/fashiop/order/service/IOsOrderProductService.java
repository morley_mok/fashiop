package top.shenyuboos.fashiop.order.service;

import top.shenyuboos.fashiop.order.entity.OsOrderProduct;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单明细表 服务类
 * </p>
 *
 * @author 莫尚荣
 * @since 2018-11-16
 */
public interface IOsOrderProductService extends IService<OsOrderProduct> {

}
