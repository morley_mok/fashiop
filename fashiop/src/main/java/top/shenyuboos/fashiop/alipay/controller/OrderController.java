package top.shenyuboos.fashiop.alipay.controller;

import java.util.Date;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alipay.api.AlipayApiException;

import top.shenyuboos.fashiop.alipay.bean.AlipayBean;
import top.shenyuboos.fashiop.alipay.service.PayService;

/* 订单接口 */
@RestController()
@RequestMapping("order")
public class OrderController {

	@Resource
	private PayService payServiceImpl;// 调用支付服务

//阿里支付
@GetMapping(value = "alipay")
public String alipay(/*String out_trade_no,String subject,String total_amount,String body*/) throws AlipayApiException {
	String out_trade_no;
	String subject;
	String total_amount;
	String body;
	out_trade_no="2018112512344535448484"+new Date();
	subject="测试订单";
	total_amount="2210";
	body="测试";
	return payServiceImpl.aliPay(new AlipayBean().setBody(body)
			.setOut_trade_no(out_trade_no)
			.setTotal_amount(new StringBuffer().append(total_amount))
			.setSubject(subject));
}
}
