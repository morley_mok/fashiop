package top.shenyuboos.fashiop.alipay.service.impl;

import org.springframework.stereotype.Service;

import com.alipay.api.AlipayApiException;

import top.shenyuboos.fashiop.alipay.bean.AlipayBean;
import top.shenyuboos.fashiop.alipay.config.AlipayUtil;
import top.shenyuboos.fashiop.alipay.service.PayService;

/* 支付服务 */
@Service(value = "alipayOrderService")
public class PayServiceImpl implements PayService {
	@Override
	public String aliPay(AlipayBean alipayBean) throws AlipayApiException {
		return AlipayUtil.connect(alipayBean);
	}
}
