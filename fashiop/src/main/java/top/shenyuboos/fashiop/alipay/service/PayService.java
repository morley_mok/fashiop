package top.shenyuboos.fashiop.alipay.service;

import com.alipay.api.AlipayApiException;

import top.shenyuboos.fashiop.alipay.bean.AlipayBean;

/*支付服务*/
public interface PayService {
	//支付宝
	String aliPay(AlipayBean alipayBean) throws AlipayApiException;
}
