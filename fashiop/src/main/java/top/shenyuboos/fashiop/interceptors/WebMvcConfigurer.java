package top.shenyuboos.fashiop.interceptors;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SuppressWarnings("deprecation")
@Configuration
public class WebMvcConfigurer extends WebMvcConfigurerAdapter {
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		 registry.addInterceptor(new LoginInterceptor())
         //添加需要验证登录用户操作权限的请求
         .addPathPatterns("/admin/**")
         //排除不需要验证登录用户操作权限的请求
         .excludePathPatterns("/admin/css/**")
         .excludePathPatterns("/admin/fonts/**")
         .excludePathPatterns("/admin/images/**")
         .excludePathPatterns("/admin/js/**")
         .excludePathPatterns("/admin/lib/**")
         .excludePathPatterns("/css/**")
         .excludePathPatterns("/Fashiop-doc/**")
         .excludePathPatterns("/fonts/**")
         .excludePathPatterns("/img/**")
         .excludePathPatterns("/js/**")
         .excludePathPatterns("/scss/**")
         .excludePathPatterns("/vendors/**")
         .excludePathPatterns("/user/cms-user/**");
	}
	
}
