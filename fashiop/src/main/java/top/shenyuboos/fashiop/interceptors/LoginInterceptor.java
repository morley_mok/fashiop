package top.shenyuboos.fashiop.interceptors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.HandlerInterceptor;

/**
 * 后台页面必须先登录再使用
 * @author 莫尚荣
 *
 */
public class LoginInterceptor implements HandlerInterceptor{
	
	private static final Logger log = Logger.getLogger(LoginInterceptor.class);
	
	/**
	 * 在请求处理前掉用
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		log.info("后台请求-----------------> 进入拦截器");
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			log.info("用户已经登录，直接进入！");
			log.info("user=" + session.getAttribute("user"));
			return true;
		}
		
		response.sendRedirect(request.getContextPath()+"/user/cms-user/login");
		
		return false;
	}
}
