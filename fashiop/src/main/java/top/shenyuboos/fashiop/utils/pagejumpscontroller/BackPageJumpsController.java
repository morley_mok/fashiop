package top.shenyuboos.fashiop.utils.pagejumpscontroller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import top.shenyuboos.fashiop.user.entity.OsUser;
import top.shenyuboos.fashiop.user.service.IOsUserService;

/**
 * 后台左侧菜单跳转Controller
 */
@Controller
@RequestMapping({"/admin","/"})
public class BackPageJumpsController {
	
	/*@Resource
	IOsUserService osUserService;
	@RequestMapping("member-list")
	public String member_list(Model model){
		List<OsUser> users=osUserService.getAllUser();
		model.addAttribute("users", users);
		for (int i = 0; i < users.size(); i++) {
			System.out.println(users.get(i));
		}
		return "admin/member-list";
	}*/
	
	@RequestMapping("echarts1")
	public String echarts1() {
		return "admin/echarts1";
	}
	
	@RequestMapping("member-del")
	public String member_del() {
		return "admin/member-del";
	}
	
	@RequestMapping("member-kiss")
	public String member_kiss() {
		return "admin/member-kiss";
	}
	
	@RequestMapping("member-view")
	public String member_view() {
		return "admin/member-view";
	}
	
	@RequestMapping("member-add")
	public String add() {
		return "admin/member-add";
	}
	
	
}
