package top.shenyuboos.fashiop.utils;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;

/**
 * @author JoelJhou
 *
 */
@EnableTransactionManagement(proxyTargetClass = true)
@Configuration
//@MapperScan("com.baomidou.cloud.service.*.mapper*")
@MapperScan("top.shenyuboos.fashiop.*.mapper.*")
public class MybatisPlusConfig {
	
	/**
     * 分页插件
     */
	@Bean
	public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }
	
}
