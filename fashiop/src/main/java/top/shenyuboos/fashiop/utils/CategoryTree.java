package top.shenyuboos.fashiop.utils;

import java.util.List;

import lombok.Data;

@Data
public class CategoryTree {
	/**
	 * 分类ID
	 */
	private Long categoryId;

	/**
	 * 子分类
	 */
	private List<CategoryTree> children;

	/**
	 * 分类名称
	 */
	private String name;

}
