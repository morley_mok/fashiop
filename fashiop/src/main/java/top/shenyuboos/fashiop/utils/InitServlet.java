package top.shenyuboos.fashiop.utils;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * 服务器初始化
 * @author 莫尚荣
 *
 */
@Component
public class InitServlet implements CommandLineRunner {

	@Autowired
    private ServletContext servletContext;
	
	@Override
	public void run(String... args) throws Exception {
		servletContext.setAttribute("startTime", System.currentTimeMillis());
		System.err.println("启动时间：" + servletContext.getAttribute("startTime"));
	}

}
