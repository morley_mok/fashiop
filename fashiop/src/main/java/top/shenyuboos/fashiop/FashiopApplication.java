package top.shenyuboos.fashiop;

import javax.servlet.MultipartConfigElement;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.util.unit.DataSize;

/**
 * 应用程序入口类
 * 
 * @author 莫尚荣
 *
 */
@SpringBootApplication
@ServletComponentScan
@EnableCaching
@MapperScan("top.shenyuboos.fashiop.*.mapper")
public class FashiopApplication extends SpringBootServletInitializer {
	
	public static void main(String[] args) {
		SpringApplication.run(FashiopApplication.class, args);
	}
	
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        // 注意这里要指向原先用main方法执行的Application启动类
        return builder.sources(FashiopApplication.class);
    }
	
	/**  
               * 文件上传配置  
     * @return  
     */  
    @Bean  
    public MultipartConfigElement multipartConfigElement() {  
        MultipartConfigFactory factory = new MultipartConfigFactory();  
        //单个文件最大  
        DataSize maxFileSize = DataSize.ofMegabytes(10);
        factory.setMaxFileSize(maxFileSize); //MB  
        /// 设置总上传数据总大小  
        DataSize maxRequestSize = DataSize.ofMegabytes(100);
        factory.setMaxRequestSize(maxRequestSize);  
        return factory.createMultipartConfig();  
    } 

}
